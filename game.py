import sys
from typing import List, Callable, Optional
from argparse import ArgumentParser

from age import AGE, Settings, Texture, Gamepad

import shared
from constants import THIS_DIR, BTN_START
from sound import Sound
from music import Music
from controller import Controller, KeyboardController, GamepadController
from text import Text
from camera import Camera
from entity_manager import EntityManager
from world import World
from hud import Hud
from inventory import Inventory
from player import Player
from editors import MapEditor, TextureMapper
from texture_map import load_texture_map
import items


### TODO ###
from slime import Slime


class Game:
    w: int = 20 * 16
    h: int = 14 * 16
    scale: int

    age: AGE = None
    scroll_tex: Texture
    light_tex: Texture
    main_tex: Texture

    sound: Sound
    music: Music
    controller: Controller
    text: Text
    cam: Camera
    ent_man: EntityManager
    hud: Hud
    inventory: Inventory
    world: World
    player: Player = None

    map_editor: Optional[MapEditor] = None # only available in debug mode
    texture_mapper: Optional[TextureMapper] = None # only available in debug mode

    frame_counter: int
    _running: bool
    _update: Callable[[], None]

    def setup(self, scale:int=3, use_gamepad:int=-1, list_gamepads:bool=False, debug:bool=False, **_kwargs):
        self.scale = scale
        self.age = AGE(Settings(
            title="Adventure",
            canvas_res=(self.w * scale, self.h * scale),
            use_gamepads=True,
            audio_buffer_size=2,
            window_resizable=True # FIXME
        ))
        self.age.win.set_min_size(self.w, self.h)
        self.keyboard = self.age.win.keyboard # only use keyboard input of the main window

        self.scroll_tex = Texture.create(self.w + 1, self.h + 1)
        self.light_tex = Texture.create(self.w + 1, self.h + 1)
        self.light_tex.set_blending(True)

        self.main_tex = Texture.load(THIS_DIR.joinpath("images.png"))

        self.age.audio.set_sounds_volume(0.7)
        self.age.audio.set_music_volume(0.58)
        self.sound = Sound(self.age.audio)
        self.sound.load_all()
        self.music = Music(self.age.audio)
        self.music.load_all()
        self.text = Text()
        self.text.load_font()

        if use_gamepad >= 0 and self.age.gamepads.get_num_available() > use_gamepad:
            gpad = Gamepad(use_gamepad)
            gpad.set_stick_as_dpad()
            self.controller = GamepadController(gpad)
        else:
            self.controller = KeyboardController(self.keyboard)

        self._running = False

        shared.setup(self)
        load_texture_map()

        # TODO: should be handled in game state objects
        if list_gamepads:
            self._update = self._update_controller_list
            self._controller_list: List[str] = [
                self.age.gamepads.get_name(i)
                for i in range(self.age.gamepads.get_num_available())
            ]
        else:
            self._update = self._default_update

            self.hud = Hud()
            self.inventory = Inventory()
            self.cam = Camera(self.w, self.h - self.hud.h)
            self.ent_man = EntityManager()
            self.world = World()
            self.world.seed = 356
            self.world.set_level(0)

            self.player = Player(-1, -1)
            self.player.center_x = self.world.center_x - 16 * 16
            self.player.tile_y = self.world.tile_h - 9
            self.player.spawn()
            self.cam.center = self.player.center

            self.ent_man.add(Slime((self.player.tile_x + 3) * 16 + 8, self.player.tile_y * 16 + 8))

            if debug:
                self.map_editor = MapEditor()
                self.texture_mapper = TextureMapper()

                #self.inventory.add(items.DESTROYER)
                self.inventory.add(items.BOMB, 30)
                #self.inventory.add(items.KEY, 5)
                self.inventory.add(items.ROCK, 10)
                self.inventory.add(items.STICK, 10)
                self.inventory.add(items.PICKAXE)

    def run(self):
        self.frame_counter = 0
        target_fps_secs = 1 / 60
        prev_time = 0.0

        paused = False

        self._running = True
        while self._running:
            self.age.update()
            if self.age.quit_requested() or self.keyboard.down_event("escape"):
                self.stop()
                return

            if self.controller.down(BTN_START): paused = not paused

            t = self.age.get_runtime()
            dt = (t - prev_time) / target_fps_secs
            prev_time = t

            if paused:
                text = "Paused"
                w, h = self.text.size(text)
                with self.age.paint.target_texture(self.light_tex):
                    self.age.paint.fill((0, 0, 0, 200))
                    self.text.render(text, (self.w - w) // 2, (self.h - h) // 2)
                for tex in (self.scroll_tex, self.light_tex):
                    self.age.paint.texture(tex, 0, 0,
                        w=self.w * self.scale, h=self.h * self.scale
                    )
            else:
                self._update()
            self.frame_counter += 1
            self.age.paint.show()

            if self.texture_mapper is not None:
                if self.keyboard.down_event("9"): self.texture_mapper.toggle_active()
                if self.texture_mapper.is_active():
                    self.texture_mapper.update(dt)
                    self.texture_mapper.render()

            if self.map_editor is not None:
                if self.keyboard.down_event("0"): self.map_editor.toggle_active()
                if self.map_editor.is_active():
                    self.map_editor.update(dt)
                    self.map_editor.render()

            #self.age.wait(1/24)

    def stop(self):
        self._running = False

    def quit(self):
        if self.age is not None: self.age.quit()

    def _default_update(self):
        # update
        self.controller.update()
        self.inventory.update()
        self.ent_man.update()
        shared.tile_animator.update()
        self.cam.update(self.player, self.world)

        # render
        self.age.paint.fill((0, 0, 0))

        self.ent_man.render_light()
        with self.age.paint.target_texture(self.scroll_tex):
            self.world.render()
            self.ent_man.render()
            self.age.paint.set_cam_pos(0, 0)
            self.age.paint.texture(self.light_tex, 0, 0)
        self.cam.render()

        self.hud.render()

    def _update_controller_list(self):
        if self.keyboard.down_event("space"):
            self._controller_list = [
                self.age.gamepads.get_name(i)
                for i in range(self.age.gamepads.get_num_available())
            ]

        with self.age.paint.target_texture(self.scroll_tex):
            self.age.paint.fill((30, 30, 50))
            x, y = 8, 8
            self.text.render("Press SPACE to refresh", x, y); y += 8
            self.text.render("----------------------", x, y); y += 8
            if len(self._controller_list) == 0:
                self.text.render("No controllers available", x, y)
            else:
                for num, name in enumerate(self._controller_list):
                    self.text.render(f"{num} : {name}", x, y)
                    y += 8

        self.age.paint.fill((0, 0, 0))
        self.age.paint.texture(self.scroll_tex,
            0, 0,
            w=self.w*self.scale, h=self.h*self.scale,
            src=(0, 0, self.w, self.h)
        )


def main(argv: List[str]):
    arg_parser = ArgumentParser("adventure", description="2D top down adventure game")
    arg_parser.add_argument("--pixel-scale", "-s", dest="scale", type=int, default=4, help="Pixel scale")
    arg_parser.add_argument("--gamepad", "-g", dest="use_gamepad", type=int, default=-1, help="Which controller to use. -1 for keyboard")
    arg_parser.add_argument("--list-gamepads", "-l", dest="list_gamepads", action="store_true", help="List available controllers")
    arg_parser.add_argument("--debug", "-d", dest="debug", action="store_true", help="Debug mode")

    args = arg_parser.parse_args(argv)
    args_dict = vars(args)

    game = Game()
    try:
        game.setup(**args_dict)
        game.run()
    finally:
        game.quit()


if __name__ == "__main__":
    main(sys.argv[1:])
