from random import uniform as rand, randint

from entity import Entity
from images import ani_twinkle_particle


class TwinkleParticle(Entity):
    def __init__(self, cx: int, cy: int, vel_fac:float=1.0):
        super().__init__(0, 0, w=8, h=8)
        self.center = cx, cy

        self._vel_x = rand(-1, 1) * vel_fac
        self._vel_y = rand(-1, 1) + vel_fac
        self._acc = 0.97
        self._vel_z = rand(0.1, 0.4)
        self._gravity = -0.001

        self._ani = ani_twinkle_particle
        self._frame_counter = 0
        self._ani_offset = randint(0, 10)

        self._alive_frames = randint(1 * 60, 2 * 60)

        self._bound_to_world = False

    def check_health(self):
        if self._frame_counter >= self._alive_frames:
            self.die()

    def set_frame(self):
        self._frame = self._ani.get_frame(self._frame_counter + self._ani_offset)
        self._frame_counter += 1

    def _move_x(self):
        self.xf += self._vel_x
        self._vel_x *= self._acc
    def _move_y(self):
        self.yf += self._vel_y
        self._vel_y *= self._acc

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass
