from pathlib import Path


THIS_DIR         = Path(__file__).parent.resolve()
SOUNDS_DIR       = Path(THIS_DIR, "sounds")
MUSIC_DIR        = Path(THIS_DIR, "music")
FONT_FILE        = Path(THIS_DIR, "font.png")
TEXTURE_MAP_FILE = Path(THIS_DIR, "texture_map.json")
LEVELS_DIR       = Path(THIS_DIR, "levels")


INF = 9999999


BTN_LEFT   = 0
BTN_RIGHT  = 1
BTN_UP     = 2
BTN_DOWN   = 3
BTN_A      = 4
BTN_B      = 5
BTN_START  = 6
BTN_SELECT = 7
BTN_L      = 8
BTN_R      = 9


DIR_RIGHT = 0
DIR_DOWN  = 1
DIR_LEFT  = 2
DIR_UP    = 3
DIRS = ((1, 0), (0, 1), (-1, 0), (0, -1))


FLOOR_LAYER = 0
TILE_LAYER  = 1

TILE_PASSED    = 0
TILE_NOT_HIT   = 1
TILE_HIT       = 2
TILE_DESTROYED = 3

HIT     = 0
NOT_HIT = 1
KILLED  = 2


STATE_IDLE      = 0
STATE_MOVING    = 1
STATE_ATTACKING = 2
STATE_ACTION    = 3
STATE_KNOCKBACK = 4
STATE_ANIMATION = 5
