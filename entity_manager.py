from typing import List, Tuple, Set

import shared
from entity import Entity


class EntityManager:
    def __init__(self):
        self._entities: List[Entity] = []
        self._collision_entities: List[Entity] = []
        self._lights: List[Tuple[int, int, int]] = []

        self._remove_q: Set[Entity] = set()

    def add(self, entity: Entity):
        if entity in self._entities: return
        self._entities.append(entity)
        if entity.check_collisions: self._collision_entities.append(entity)

    def remove(self, entity: Entity):
        if entity not in self._entities: return
        self._remove_q.add(entity)

    def clear(self):
        self._entities.clear()
        self._lights.clear()
        self._remove_q.clear()
        self._collision_entities.clear()

    def update(self):
        for entity in self._entities:
            entity.update()

        for entity in self._entities:
            entity.move()

        self._check_collisions()

        for entity in self._entities:
            entity.action()

        for entity in self._entities:
            entity.check_health()

        if len(self._remove_q) > 0:
            for entity in self._remove_q:
                self._entities.remove(entity)
                if entity.check_collisions: self._collision_entities.remove(entity)
            self._remove_q.clear()

        self._lights.clear()
        for entity in self._entities:
            entity.set_frame()
            if entity.light_radius > 0:
                self._lights.append((
                    entity.center_x, int(entity.center_y - entity.z),
                    int(entity.light_radius)
                ))

    def render(self):
        for entity in sorted(self._entities, key=lambda ent: ent.b + ent.sort_off):
            entity.render()

    def render_light(self):
        with shared.paint.target_texture(shared.game.light_tex):
            darkness = shared.game.world.darkness
            shared.paint.fill((0, 0, 0, shared.game.world.darkness))
            shared.paint.blend_paint(False)

            paint_circle = shared.paint.circle
            outer_darkness = 130
            inner_darkness = 0
            outer_color = (0, 0, 0, outer_darkness)
            inner_color = (0, 0, 0, inner_darkness)

            if darkness > outer_darkness:
                for x, y, radius in self._lights:
                    paint_circle(outer_color, x, y, radius)
            if darkness > inner_darkness:
                for x, y, radius in self._lights:
                    paint_circle(inner_color, x, y, int(radius * 0.8))

            shared.paint.blend_paint(True)

    def _check_collisions(self):
        n_entities = len(self._collision_entities)
        for subj_i in range(0, n_entities - 1):
            subj = self._collision_entities[subj_i]
            for obj_i in range(subj_i + 1, n_entities):
                obj = self._collision_entities[obj_i]
                if not subj.overlaps(obj): continue
                subj.on_collision(obj)
                obj.on_collision(subj)
