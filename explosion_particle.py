from random import uniform as rand

import shared
from entity import Entity
from frame import Frame
from images import ani_explosion, ani_fire_explosion, img_empty


class ExplosionParticle(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(0, 0)
        self.center = (cx, cy)

        self._img_align = self.align_c
        self.sort_off = 1000

        self._ani = ani_explosion

        self._frame_count = 0

    def check_health(self):
        if self._ani.is_done(self._frame_count):
            shared.game.cam.shake(18, 0.9)
            self.die()

    def set_frame(self):
        self._frame = self._ani.get_frame(self._frame_count)
        self._frame_count += 1

    def move(self): pass


class FireExplosionParticle(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(0, 0)
        self.center = (cx, cy)

        self._vel_z = rand(0, 0.7)

        self._ani = ani_fire_explosion
        self._img_align = self.align_c
        self._frame = img_empty

        self.angle = rand(0, 360)

        self.light_radius = 0
        self._delta_light_radius = 4

        self._frame_count = int(rand(-0.2 * 60, 14))

    def check_health(self):
        if self._ani.is_done(self._frame_count):
            self.die()

    def set_frame(self):
        self._frame_count += 1
        if self._frame_count >= 0:
            self._frame = self._ani.get_frame(self._frame_count)
        self.light_radius += self._delta_light_radius
        if self.light_radius >= 50:
            self.light_radius = 50
            self._delta_light_radius = -0.8

    def move(self):
        self.z += self._vel_z

    def render(self):
        frame = self._frame
        img = self._img_rect

        img.size = frame.size
        self._img_align(img, self)
        shared.paint.mod_texture(shared.main_tex,
            img.x + frame.off_x,
            img.y + frame.off_y - int(self.z),
            src=frame.get_rect(),
            angle=self.angle
        )


class ShockwaveParticle(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(0, 0, 8, 8)
        self.center = (cx, cy)

        vel = 3.4
        self._alive_frames = 0.2 * 60
        
        angle = rand(0, 1)
        dir_ = 1j ** (angle * 4)
        self._vel_x = dir_.real * vel
        self._vel_y = dir_.imag * vel

        self._frame = Frame(9,10, angle=angle * 360)

    def move(self):
        self.xf += self._vel_x
        self.yf += self._vel_y

    def check_health(self):
        if self._alive_frames <= 0:
            self.die()
        self._alive_frames -= 1
