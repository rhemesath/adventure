from typing import Protocol, Optional, Union, Tuple

from age import Painter, Texture, Mouse

from rect import Rect
from tile import Tile
from text import Text


class CamRect(Rect):
    """ Rect with variable tile size """
    def __init__(self, x: int, y: int, w: int, h: int, tilesize: float):
        super().__init__(x, y, w, h)
        self.tilesize = tilesize
        self._drag_start_x = 0.0
        self._drag_start_y = 0.0

    def zoom_tilesize(self, ts: float, real_target_x: int, real_target_y: int):
        if ts < 1.0: ts = 1.0
        if ts == self.tilesize: return

        target_x = (self.xf + real_target_x) / self.ts
        target_y = (self.yf + real_target_y) / self.ts
        self.ts = ts
        # adjust position
        self.x = target_x * self.ts - real_target_x
        self.y = target_y * self.ts - real_target_y

    def update(self, mouse: Mouse, dt: float):
        if mouse.down_event("middle"):
            self._drag_start_x = self.xf + mouse.x
            self._drag_start_y = self.yf + mouse.y
        elif mouse.is_pressed("middle"):
            self.x = self._drag_start_x - mouse.x
            self.y = self._drag_start_y - mouse.y

        ts = self.tilesize
        ts -= mouse.scroll_y() * ts / 20 * dt
        self.zoom_tilesize(ts, mouse.x, mouse.y)

    def real_to_tile_x(self, real_x: int) -> int:
        return (self.x + real_x) // self.ts

    def real_to_tile_y(self, real_y: int) -> int:
        return (self.y + real_y) // self.ts

    @property
    def ts(self) -> int: return int(self.tilesize)
    @property
    def tile_x(self) -> int: return self.x // self.ts
    @property
    def tile_y(self) -> int: return self.y // self.ts
    @property
    def tile_ri(self) -> int: return self.r // self.ts
    @property
    def tile_bi(self) -> int: return self.b // self.ts

    @ts.setter
    def ts(self, ts: float): self.tilesize = ts
    @tile_x.setter
    def tile_x(self, tile_x: int): self.x = tile_x * self.ts
    @tile_y.setter
    def tile_y(self, tile_y: int): self.y = tile_y * self.ts
    @tile_ri.setter
    def tile_ri(self, tile_ri: int): self.r = tile_ri * self.ts
    @tile_bi.setter
    def tile_bi(self, tile_bi: int): self.r = tile_bi * self.ts


class Layer(Protocol):
    visible: bool

    def get_tile(self, tile_x: int, tile_y: int) -> Optional[Union[Tile, str]]:
        ...

    def set_tile(self, tile: Optional[Union[Tile, str]], tile_x: int, tile_y: int):
        ...

    def render(self, renderer: Union[Texture, Text], cam: CamRect):
        ...

    def tile_exists(self, tile_x: int, tile_y: int) -> bool:
        ...


class Cursor:
    def __init__(self):
        self.tile_x = 0
        self.tile_y = 0
        self.tile: Optional[Union[Tile, str]]
        self.tile_index = 0
        self.tile_at: Optional[Union[Tile, str]]
        self.tile_ok = True
        self.pos_valid = True

        self._last_set_pos: Optional[Tuple[int, int]] = None

        self.set_tile(None)

    def update(self, layer: Layer, cam: CamRect, mouse: Mouse):
        self.tile_x = cam.real_to_tile_x(mouse.x)
        self.tile_y = cam.real_to_tile_y(mouse.y)

        self._update_tile_ok(layer)
        self.pos_valid = (
            cam.point_inside(mouse.x + cam.x, mouse.y + cam.y) and
            layer.tile_exists(self.tile_x, self.tile_y)
        )

        if mouse.up_event("left") or mouse.up_event("right") or not self.pos_valid:
            self._last_set_pos = None

        if self.pos_valid:
            if mouse.is_pressed("left") and self.tile_ok:
                self._add_line(self.tile, layer)

            elif mouse.is_pressed("right"):
                layer.set_tile(None, self.tile_x, self.tile_y)

        self._update_tile_at(layer)

    def set_tile(self, tile: Optional[Union[Tile, str]]):
        self.tile = tile

    def render(self, paint: Painter, cam: CamRect):
        if not self.pos_valid or not self.tile_ok: return
        paint.rect((255, 255, 255, 70),
            self.tile_x * cam.ts, self.tile_y * cam.ts, cam.ts, cam.ts
        )

    def _update_tile_ok(self, layer: Layer):
        self.tile_ok = True

    def _update_tile_at(self, layer: Layer):
        self.tile_at = layer.get_tile(self.tile_x, self.tile_y)

    def _add_line(self, tile: Optional[Union[Tile, str]], layer: Layer):
        if self._last_set_pos is None:
            self._last_set_pos = (self.tile_x, self.tile_y)

        x0, y0 = self._last_set_pos
        sx = 1 if x0 < self.tile_x else -1
        sy = 1 if y0 < self.tile_y else -1
        dx =  abs(self.tile_x - x0)
        dy = -abs(self.tile_y - y0)
        err = dx + dy
        while True:
            layer.set_tile(tile, x0, y0)
            if x0 == self.tile_x and y0 == self.tile_y: break
            e2 = err * 2
            if e2 >= dy:
                err += dy
                x0 += sx
            if e2 <= dx:
                err += dx
                y0 += sy
        self._last_set_pos = (x0, y0)
