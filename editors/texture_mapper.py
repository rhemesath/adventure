from typing import List, Callable, Optional

from age import Window, Texture, Painter, AntiAliasing, Keyboard

import shared
from rect import Rect
from text import Text
from tiles import predefined_tiles
from tile import TileType
from texture_map import texture_map, save_texture_map

from .editor_tk import TextWidget, Label, RadioButton, RadioMenu, Button
from .elements import CamRect, Cursor, Layer


def get_predefined_id(gfx_index: int) -> str:
    aliases = predefined_tiles.get(gfx_index, [])
    if len(aliases) == 1: return aliases[0].type_.id
    elif len(aliases) > 1: return "[...]"
    return ""


class TileTypeLayer(Layer):
    def __init__(self, tile_w: int, tile_h: int):
        self.visible = True

        self.tile_w = tile_w
        self.tile_h = tile_h

    def set_tile(self, tile: Optional[str], tile_x: int, tile_y: int):
        if not self.tile_exists(tile_x, tile_y): return
        if tile is None: texture_map.pop(tile_x + tile_y * self.tile_w, None)
        else: texture_map[tile_x + tile_y * self.tile_w] = tile

    def get_tile(self, tile_x: int, tile_y: int) -> Optional[str]:
        return texture_map.get(tile_x + tile_y * self.tile_w)

    def render(self, renderer: Text, cam: CamRect):
        tilesize = cam.ts

        range_x = range(cam.tile_x, cam.tile_ri + 1)
        for tile_y in range(cam.tile_y, cam.tile_bi + 1):
            if tile_y < 0: continue
            elif tile_y >= self.tile_h: break
            y = tile_y * tilesize
            row_i = tile_y * self.tile_w
            for tile_x in range_x:
                if tile_x < 0: continue
                elif tile_x >= self.tile_w: break
                index = row_i + tile_x
                tile_type_id = texture_map.get(index)
                color = None
                if tile_type_id is None:
                    tile_type_id = get_predefined_id(index)
                    if tile_type_id == "": continue
                    color = (255, 0, 0)
                size = int(tilesize * 0.9 // len(tile_type_id))
                renderer.render(tile_type_id, tile_x * tilesize, y + (tilesize - size) // 2, size=size, color=color)

    def tile_exists(self, tile_x: int, tile_y: int) -> bool:
        return 0 <= tile_x < self.tile_w and 0 <= tile_y < self.tile_h


class TextureMapperCursor(Cursor):
    def _update_tile_ok(self, layer: TileTypeLayer):
        self.tile_ok = get_predefined_id(self.tile_x + self.tile_y * layer.tile_w) == ""


class TileTypePickAction:
    def __init__(self, label: Label, select_tile_type: Callable[[str], None]):
        self.label = label
        self.select_tile_type = select_tile_type

    def __call__(self, btn: RadioButton):
        if btn.checked:
            self.label.text_color = None
            self.select_tile_type(self.label.label)
        else:
            self.label.text_color = (128, 128, 128)


class Sidebar(TextWidget):
    def __init__(self, app: "TextureMapper", w: int, cursor: Cursor):
        Rect.__init__(self, 0, 0, w, 0)

        self.setup_(app.win, app.text)
        self.parent = app.win_rect

        self._labels: List[Label] = []

        tile_type_buttons: List[RadioButton] = []
        start_x = 8
        y = 10
        pad = 2
        size = 12
        for tile_type_id in TileType.all_:
            x = start_x
            label = Label(self, tile_type_id, x=0, y=y, text_size=size)

            btn = RadioButton(self, x=x, y=y, text_size=size,
                on_check=TileTypePickAction(label, cursor.set_tile)
            )
            tile_type_buttons.append(btn)
            x += btn.w + pad

            label.x = x
            self._labels.append(label)

            y += btn.h + pad

        self.radio_menu = RadioMenu(tile_type_buttons)

    def update(self, keyboard: Keyboard):
        if   keyboard.down_event("w"): self.radio_menu.check_prev()
        elif keyboard.down_event("s"): self.radio_menu.check_next()

        self.radio_menu.update()

    def render(self):
        self.paint.rect((50, 50, 70), 0, 0, *self.size)

        self.radio_menu.render()
        for label in self._labels: label.render()

    def update_rect_(self):
        self.r = self.parent.r
        self.h = self.parent.h


class Menubar(TextWidget):
    def __init__(self, app: "TextureMapper", h: int):
        Rect.__init__(self, 0, 0, 0, h)

        self.setup_(app.win, app.text)

        self.buttons: List[Button] = [
            Button(self, "Save", x=4, text_size=12, on_click=lambda _btn: save_texture_map())
        ]

    def update(self):
        for button in self.buttons: button.update()

    def render(self):
        self.paint.rect((40, 40, 60), 0, 0, *self.size)

        for button in self.buttons: button.render()

    def update_rect_(self):
        for button in self.buttons:
            button.center_y = self.h // 2


class TextureMapper:
    win: Window
    paint: Painter

    text: Text
    main_tex: Texture

    cam: CamRect
    cursor: TextureMapperCursor
    layer: TileTypeLayer

    win_rect: Rect
    sidebar: Sidebar
    menubar: Menubar

    _active: bool
    _is_set_up: bool

    def __init__(self):
        self.win_rect = Rect(0, 0, 1000, 700)
        self.win = Window("Texture Mapper", *self.win_rect.size, resizable=True)

        self._active = False
        self._is_set_up = False

    def update(self, dt: float):
        if self.win.keyboard.down_event("escape"):
            self.deactivate()
            return

        mouse = self.win.mouse
        og_my = mouse.y
        mouse.y -= self.menubar.h
        self.cam.update(mouse, dt)
        self.cursor.update(self.layer, self.cam, mouse)
        mouse.y = og_my

        self.menubar.update()
        self.sidebar.update(self.win.keyboard)

    def render(self):
        self.paint.fill((30, 30, 50))

        og_cam_pos = Painter.get_cam_pos()

        Painter.set_cam_pos(self.cam.x, self.cam.y - self.menubar.h)
        w, h = self.layer.tile_w * self.cam.ts, self.layer.tile_h * self.cam.ts
        self.paint.rect((127, 127, 127), 0, 0, w, h)
        self.paint.texture(self.main_tex, 0, 0, w=w, h=h)
        self.layer.render(self.text, self.cam)
        self.cursor.render(self.paint, self.cam)

        Painter.set_cam_pos(-self.menubar.x, -self.menubar.y)
        self.menubar.render()

        Painter.set_cam_pos(-self.sidebar.x, -self.sidebar.y)
        self.sidebar.render()

        self.paint.show()
        Painter.set_cam_pos(*og_cam_pos)

    def activate(self):
        self._active = True
        self.win.show()
        if not self._is_set_up: self._setup()

    def deactivate(self):
        self._active = False
        self.win.hide()

    def toggle_active(self):
        if self._active: self.deactivate()
        else: self.activate()

    def is_active(self) -> bool:
        return self._active

    def _on_win_close(self) -> bool:
        self.deactivate()
        return False

    def _on_win_size_changed(self):
        self.win_rect.size = self.win.get_size()
        self.sidebar.update_rect_()

        self.cam.size = (self.win_rect.w - self.sidebar.w, self.win_rect.h - self.menubar.h)

        self.menubar.w = self.cam.w
        self.menubar.update_rect_()

    def _setup(self):
        self.paint = self.win.create_painter_()
        self.main_tex = Texture.from_texture(shared.main_tex, painter=self.paint)

        self.text = Text()
        with AntiAliasing():
            self.text.load_font(paint=self.paint)

        self.layer = TileTypeLayer(self.main_tex.w // 16, self.main_tex.h // 16)

        self.cam = CamRect(0, 0, 0, 0, 16)
        self.cursor = TextureMapperCursor()
        self.sidebar = Sidebar(self, 300, self.cursor)
        self.menubar = Menubar(self, 30)

        self.win.set_min_size(400, 300)
        self.win.do_close_ = self._on_win_close
        self.win.on_resize_ = self._on_win_size_changed

        self._on_win_size_changed()

        self._is_set_up = True
