from typing import Optional, List, ClassVar
from math import sin

from age import Window, Painter, Texture, Mouse, Keyboard, AntiAliasing
from age.types import ColorType

import shared
from tile import Tile, TileType
from rect import Rect
from text import Text
from texture_map import texture_map
from constants import TILE_LAYER, FLOOR_LAYER

from .editor_tk import CheckBox, RadioButton, RadioMenu, Label, TextWidget, Button
from .elements import CamRect, Layer, Cursor


class BaseTileLayer(Layer):
    layer: ClassVar[int]
    name:  ClassVar[str]

    def render(self, renderer: Texture, cam: CamRect):
        if self.visible: self._render(renderer, cam)

    def _render(self, main_tex: Texture, cam: CamRect):
        ...


class GroundLayer(BaseTileLayer):
    layer = FLOOR_LAYER
    name = "Ground"

    def __init__(self):
        self.visible = True

        self.get_tile = shared.game.world.get_floor_tile
        self.set_tile = shared.game.world.set_floor_tile
        self.tile_exists = shared.game.world.tile_exists

    def _render(self, main_tex: Texture, cam: CamRect):
        shared.game.world.render_ground_(
            cam.ts, main_tex, main_tex.get_painter_().texture, cam
        )


class TileLayer(BaseTileLayer):
    layer = TILE_LAYER
    name = "Layer 1"

    def __init__(self):
        self.visible = True

        self.get_tile = shared.game.world.get_tile
        self.set_tile = shared.game.world.set_tile
        self.tile_exists = shared.game.world.tile_exists

    def _render(self, main_tex: Texture, cam: CamRect):
        shared.game.world.render_layer_(
            cam.ts, main_tex, main_tex.get_painter_().texture, cam
        )


class BaseTilePicker(Rect):
    def __init__(self, parent: Rect, tile_indices: List[List[int]], x:int=0, y:int=0, tile_size:int=16):
        super().__init__(x, y, 0, 0)
        self.parent = parent
        self.indices = tile_indices
        self.ts = tile_size

        self.tile_w = len(tile_indices[0])
        self.tile_h = len(tile_indices)
        self.size = (self.tile_w * self.ts, self.tile_h * self.ts)

        self.index_x = 0
        self.index_y = 0

        self._pressed_down = False
        self._bg_color: Optional[ColorType] = (128, 128, 128)

    @staticmethod
    def on_pick(self: "BaseTilePicker", index: int): pass

    def update(self, mouse: Mouse):
        x, y = self.index_x, self.index_y

        mouse_x, mouse_y = mouse.x - self.parent.x, mouse.y - self.parent.y
        hovered = self.point_inside(mouse_x, mouse_y)
        if hovered and mouse.down_event("left"):
            self._pressed_down = True

        elif self._pressed_down and hovered and mouse.up_event("left"):
            # clicked
            x, y = (mouse_x - self.x) // self.ts, (mouse_y - self.y) // self.ts

        if not mouse.is_pressed("left"):
            self._pressed_down = False

        self.set_index_pos(x, y)

    def render(self, main_tex: Texture):
        paint = main_tex.get_painter_()

        if self._bg_color is not None:
            paint.rect(self._bg_color, *self.get_rect())

        for tile_y, line in enumerate(self.indices):
            y = self.y + tile_y * self.ts
            for tile_x, index in enumerate(line):
                x = self.x + tile_x * self.ts
                self._render_tile(main_tex, index, x, y)

        paint.rect((255, 255, 255, int(abs(sin(shared.age.get_runtime() * 2)) * 70)), self.x + self.index_x * self.ts, self.y + self.index_y * self.ts, self.ts, self.ts)

    def set_tile_indices(self, tile_indices: List[List[int]]):
        self.indices = tile_indices
        self.tile_w = len(tile_indices[0])
        self.tile_h = len(tile_indices)
        self.size = (self.tile_w * self.ts, self.tile_h * self.ts)
        self.index_x, self.index_y = 0, 0
        self.update_rect_()
        self.on_pick(self, 0)

    def set_index_pos(self, x: int, y: int):
        if (x, y) == (self.index_x, self.index_y): return
        self.index_x, self.index_y = x, y
        self.on_pick(self, self.indices[y][x])

    def update_rect_(self):
        self.center_x = self.parent.w // 2

    def _render_tile(self, texture: Texture, index: int, x: int, y: int):
        tex_w = texture.w // 16
        texture.get_painter_().texture(texture, x, y, w=self.ts, h=self.ts, src=(
            (index % tex_w) * 16, (index // tex_w) * 16, 16, 16
        ))


class TilePicker(BaseTilePicker):
    def update(self, mouse: Mouse, keyboard: Keyboard):
        super().update(mouse)

        x, y = self.index_x, self.index_y
        if keyboard.down_event("a"):
            x -= 1
            if x < 0: x = 0
        if keyboard.down_event("d"):
            x += 1
            if x >= self.tile_w: x = self.tile_w - 1
        if keyboard.down_event("w"):
            y -= 1
            if y < 0: y = 0
        if keyboard.down_event("s"):
            y += 1
            if y >= self.tile_h: y = self.tile_h - 1

        self.set_index_pos(x, y)


class TilePreview(BaseTilePicker):
    alias_id: int = 0
    cursor: Cursor

    def set_alias_id(self, alias_id: int):
        self.alias_id = alias_id

    def update(self, mouse: Mouse, cursor: Cursor):
        self.cursor = cursor
        super().update(mouse)

    def _render_tile(self, texture: Texture, index: int, x: int, y: int):
        paint = texture.get_painter_()

        tile: Optional[Tile] = None
        aliases = Tile.aliases.get(self.alias_id)
        if aliases is not None:
            tile = aliases[index]

        # current tile
        if tile is not None: tile.render(texture, x, y, size=self.ts)
        # draw red X if tile cannot be placed
        if not self.cursor.tile_ok:
            for off_y in range(2):
                paint.line((255, 0, 0), x, y + off_y, x + self.ts, y + self.ts + off_y)
                paint.line((255, 0, 0), x, y + off_y + self.ts, x + self.ts, y + off_y)


class Sidebar(TextWidget):
    def __init__(self, parent: Rect, w: int, cursor: Cursor, map_editor: "MapEditor"):
        Rect.__init__(self, 0, 0, w, 0)

        self.setup_(map_editor.win, map_editor.text)

        self.parent = parent
        self.cursor = cursor

        tex_w, tex_h = shared.main_tex.w // 16, shared.main_tex.h // 16
        self.tile_picker  = TilePicker(self, [[y * tex_w + x for x in range(tex_w)] for y in range(tex_h)], y=8, tile_size=16)
        self.tile_preview = TilePreview(self, [[0]], y=300, tile_size=64)
        self.tile_picker.on_pick  = self._on_tile_picker_pick
        self.tile_preview.on_pick = self._on_tile_preview_pick

        self.layer_visible_buttons: List[CheckBox]    = []
        self.labels = []

        layer_select_buttons: List[RadioButton] = []
        text_size = 14
        y = 400
        pad = 2
        for layer in reversed(map_editor.layers):
            label = Label(self, layer.name, x=0, y=y, text_size=text_size)

            x = 8
            radio_btn = RadioButton(self, x=x, y=y, text_size=text_size)
            radio_btn.layer = layer
            radio_btn.layer_label = label
            def on_check(b: RadioButton):
                if b.checked:
                    map_editor.set_layer(b.layer)
                    b.layer_label.text_color = None
                else:
                    b.layer_label.text_color = (128, 128, 128)

            radio_btn.on_check = on_check
            layer_select_buttons.append(radio_btn)
            x += radio_btn.w + pad

            check_box = CheckBox(self, x=x, y=y, text_size=text_size)
            check_box.layer = layer
            def on_check(b: CheckBox): b.layer.visible = b.checked
            check_box.on_check = on_check
            check_box.set_checked(True)
            self.layer_visible_buttons.append(check_box)
            x += check_box.w + pad

            label.x = x
            self.labels.append(label)

            y += check_box.h + pad

        self.radio_menu = RadioMenu(layer_select_buttons)

    def update(self, keyboard: Keyboard):
        self.tile_picker.update(self.mouse, keyboard)
        self.tile_preview.update(self.mouse, self.cursor)

        for btn in self.layer_visible_buttons: btn.update()
        self.radio_menu.update()

    def render(self, main_tex: Texture):
        paint = main_tex.get_painter_()

        paint.rect((50, 50, 70), 0, 0, *self.size)
        self.tile_picker.render(main_tex)
        self.tile_preview.render(main_tex)
        self._render_tile_at()

        self.radio_menu.render()
        for btn in self.layer_visible_buttons: btn.render()
        for label in self.labels: label.render()

    def update_rect_(self):
        self.r = self.parent.r
        self.h = self.parent.h

        self.tile_picker.update_rect_()
        self.tile_preview.update_rect_()

    def _render_tile_at(self):
        size = 12
        lines = (
            f"X,Y: {self.cursor.tile_x},{self.cursor.tile_y}",
            self.cursor.tile_at.type_.id
        )
        y = self.h - size * len(lines) - 8
        for line in lines:
            self.text.render(line, 8, y, size=size)
            y += size

    def _on_tile_picker_pick(self, tile_picker: TilePicker, index: int):
        self.tile_preview.set_alias_id(index)
        aliases = Tile.aliases.get(index)
        indices = [[0]] if aliases is None else [[i for i in range(len(aliases))]]
        self.tile_preview.set_tile_indices(indices)

    def _on_tile_preview_pick(self, tile_preview: TilePreview, index: int):
        aliases = Tile.aliases.get(tile_preview.alias_id)
        if aliases is None:
            # Tile does not exist (yet). Check if it is defined
            tile_type: Optional[TileType] = TileType.all_.get(texture_map.get(tile_preview.alias_id))
            if tile_type is None: tile = None # not defined
            else: tile = Tile.new(tile_preview.alias_id, tile_type)
        else:
            tile = aliases[index]
        self.cursor.set_tile(tile)


class Menubar(TextWidget):
    def __init__(self, app: "MapEditor", h: int):
        super().__init__(0, 0, 0, h)

        self.setup_(app.win, app.text)

        self.buttons: List[Button] = [
            Button(self, "Save", x=4, on_click=lambda _btn: shared.game.world.save(), text_size=12)
        ]

    def update(self):
        for button in self.buttons: button.update()

    def render(self):
        self.paint.rect((40, 40, 60), 0, 0, *self.size)

        for button in self.buttons: button.render()

    def update_rect_(self):
        for button in self.buttons: button.center_y = self.h // 2


class MapEditorCursor(Cursor):
    def _update_tile_at(self, layer: Layer):
        self.tile_ok = self.tile is not None and self.tile.type_.layer == layer.layer

    def _update_tile_ok(self, layer: Layer):
        self.tile_at = shared.game.world.get_real_tile(self.tile_x, self.tile_y)


class MapEditor:
    win:      Window
    paint:    Painter
    keyboard: Keyboard
    mouse:    Mouse

    text:     Text
    main_tex: Texture

    cam:    CamRect
    cursor: MapEditorCursor

    layers: List[Layer]
    layer:  Layer

    win_rect: Rect
    menubar: Menubar
    sidebar:  Sidebar

    _active:    bool
    _is_set_up: bool

    def __init__(self):
        self.win_rect = Rect(0, 0, 800, 600)

        self.win = Window("Map Editor", *self.win_rect.size, resizable=True)

        self._active = False
        self._is_set_up = False

    def update(self, dt: float):
        if self.keyboard.down_event("escape"):
            self.deactivate()
            return

        self.menubar.update()
        self.sidebar.update(self.keyboard)

        og_my = self.mouse.y
        self.mouse.y -= self.menubar.h
        self.cam.update(self.mouse, dt)
        self.cursor.update(self.layer, self.cam, self.mouse)
        self.mouse.y = og_my

        if self.keyboard.down_event("p") and self.cursor.pos_valid:
            shared.game.player.tile_x = self.cursor.tile_x
            shared.game.player.tile_y = self.cursor.tile_y

        if self.keyboard.down_event("o"): shared.game.world.change_level(-1)
        if self.keyboard.down_event("l"): shared.game.world.change_level( 1)

    def render(self):
        self.paint.fill((30, 30, 50))

        # save and later restore game's camera drawing offset
        og_cam_pos = Painter.get_cam_pos()

        Painter.set_cam_pos(self.cam.x, self.cam.y - self.menubar.h)
        for layer in self.layers: layer.render(self.main_tex, self.cam)
        self.cursor.render(self.paint, self.cam)

        Painter.set_cam_pos(-self.menubar.x, -self.menubar.y)
        self.menubar.render()

        Painter.set_cam_pos(-self.sidebar.x, -self.sidebar.y)
        self.sidebar.render(self.main_tex)

        self.paint.show()
        Painter.set_cam_pos(*og_cam_pos)

    def activate(self):
        self._active = True
        self.win.show()
        if not self._is_set_up: self._setup()
    def deactivate(self):
        self._active = False
        self.win.hide()
    def toggle_active(self):
        if self._active: self.deactivate()
        else: self.activate()
    def is_active(self) -> bool: return self._active

    def set_layer(self, layer: Layer):
        self.layer = layer

    def _on_win_close(self) -> bool:
        self.deactivate()
        return False

    def _on_win_size_changed(self):
        self.win_rect.size = self.win.get_size()

        self.sidebar.update_rect_()

        self.cam.size = (self.win_rect.w - self.sidebar.w, self.win_rect.h - self.menubar.h)

        self.menubar.w = self.cam.w
        self.menubar.update_rect_()

    def _setup(self):
        self.paint = self.win.create_painter_()
        self.keyboard = self.win.keyboard
        self.mouse = self.win.mouse

        self.main_tex = Texture.from_texture(shared.main_tex, painter=self.paint)
        self.text = Text()
        with AntiAliasing():
            self.text.load_font(paint=self.paint)

        self.cam    = CamRect(0, 0, *self.win_rect.size, 16)
        self.cursor = MapEditorCursor()

        self.layers = [GroundLayer(), TileLayer()]
        self.layer  = self.layers[-1]

        self.menubar = Menubar(self, 30)
        self.sidebar = Sidebar(self.win_rect, 300, self.cursor, self)

        self.win.set_min_size(400, 300)
        self.win.do_close_ = self._on_win_close
        self.win.on_resize_ = self._on_win_size_changed

        self._on_win_size_changed()

        self._is_set_up = True
