from typing import Optional, Callable, List
from abc import ABC, abstractmethod

from age import Window, Painter, Mouse
from age.types import ColorType

from rect import Rect
from text import Text, TextColorType


class TextWidget(Rect, ABC):
    paint: Painter
    text:  Text
    mouse: Mouse

    @abstractmethod
    def render(self): pass

    def setup_(self, window: Window, text: Text):
        self.paint = window.paint
        self.mouse = window.mouse
        self.text  = text


class Label(TextWidget):
    color: ColorType = (80, 80, 100, 0)
    text_size: int = 8
    ipad: int = 4

    def __init__(self,
        parent: TextWidget,
        label: str,
        x:int=0, y:int=0,
        color:Optional[ColorType]=None, text_color:TextColorType=None,
        text_size:int=-1,
        ipad:int=-1
    ):
        Rect.__init__(self, x, y, 0, 0)

        self.paint = parent.paint
        self.mouse = parent.mouse
        self.text  = parent.text

        self.parent = parent
        self.label = label
        self.text_color = text_color
        if color is not None: self.color = color
        if text_size > 0: self.text_size = text_size
        if ipad >= 0: self.ipad = ipad

        self._update_size()

    def set_label(self, label: str):
        self.label = label
        self._update_size()

    def set_text_size(self, text_size: int):
        self.text_size = text_size
        self._update_size()

    def set_ipad(self, ipad: int):
        self.ipad = ipad
        self._update_size()

    def _update_size(self):
        iw, ih = self.text.size(self.label, self.text_size)
        self.size = (iw + self.ipad * 2, ih + self.ipad * 2)

    def render(self):
        self.paint.rect(self.color, *self.get_rect())
        self.text.render(self.label, self.x + self.ipad, self.y + self.ipad, size=self.text_size, color=self.text_color)


class Button(Label):
    color: ColorType = (80, 80, 100)

    def __init__(self,
        parent: TextWidget,
        label: str,
        x:int=0, y:int=0,
        on_click:Optional[Callable[["Button"], None]]=None,
        color:Optional[ColorType]=None, text_color:TextColorType=None,
        text_size:int=-1,
        ipad:int=-1
    ):
        super().__init__(parent, label, x=x, y=y, color=color, text_color=text_color, text_size=text_size, ipad=ipad)

        if on_click is not None: self.on_click = on_click

        self._hovered = False
        self._pressed_down = False

        self._update_size()

    @staticmethod
    def on_click(self: TextWidget): pass

    def update(self):
        self._hovered = self.point_inside(self.mouse.x - self.parent.x, self.mouse.y - self.parent.y)

        if self._hovered and self.mouse.down_event("left"):
            self._pressed_down = True

        elif self._pressed_down and self._hovered and self.mouse.up_event("left"):
            self._on_click()

        if not self.mouse.is_pressed("left"):
            self._pressed_down = False

    def render(self):
        color_tr = (120, 120, 120)
        color_bl = (20, 20, 20)

        if self._pressed_down:
            color_tr, color_bl = color_bl, color_tr

        super().render()
        self.paint.line(color_tr, self.x, self.y, self.r-1, self.y)
        self.paint.line(color_tr, self.r-1, self.y, self.r-1, self.b-1)
        self.paint.line(color_bl, self.x, self.b-1, self.r-1, self.b-1)
        self.paint.line(color_bl, self.x, self.y, self.x, self.b-1)

        if self._pressed_down:
            self.paint.rect((0, 0, 0, 60), *self.get_rect())
        elif self._hovered:
            self.paint.rect((255, 255, 255, 60), *self.get_rect())

    def _on_click(self):
        self.on_click(self)


class CheckBox(Button):
    checked_label   = "[*]"
    unchecked_label = "[ ]"

    def __init__(self,
        parent: TextWidget,
        checked:bool=False,
        x:int=0, y:int=0,
        on_check:Optional[Callable[[], None]]=None,
        color: Optional[ColorType]=None, text_color:TextColorType=None,
        text_size:int=-1, ipad:int=-1
    ):
        super().__init__(parent,
            self.unchecked_label,
            x=x, y=y, color=color, text_color=text_color, text_size=text_size, ipad=ipad
        )
        self.checked = checked
        if on_check is not None: self.on_check = on_check

        self.set_checked(checked)

    @staticmethod
    def on_check(self: "CheckBox"): pass

    def set_checked(self, checked: bool):
        self.set_label(self.checked_label if checked else self.unchecked_label)
        self.checked = checked
        self._on_check()

    def _on_click(self):
        self.set_checked(not self.checked)
        self.on_click(self)

    def _on_check(self):
        self.on_check(self)


class RadioButton(CheckBox):
    checked_label   = "(*)"
    unchecked_label = "( )"
    menu: Optional["RadioMenu"] = None

    def _on_click(self):
        self.set_checked(True)
        self.on_click(self)

    def _on_check(self):
        if self.menu is not None: self.menu.on_check_(self)
        self.on_check(self)


class RadioMenu:
    def __init__(self, radio_buttons: List[RadioButton], selected:int=0):
        self.buttons = radio_buttons
        self._checked_i = selected

        for i, button in enumerate(self.buttons):
            button.menu = self
            button.set_checked(i == selected)

    def update(self):
        for button in self.buttons: button.update()

    def render(self):
        for button in self.buttons: button.render()

    def on_check_(self, radio_button: RadioButton):
        if not radio_button.checked: return
        self._checked_i = self.buttons.index(radio_button)
        for other in self.buttons:
            if other is radio_button: continue
            other.set_checked(False)

    def check_next(self):
        self._checked_i += 1
        if self._checked_i >= len(self.buttons): self._checked_i = 0
        self.buttons[self._checked_i].set_checked(True)

    def check_prev(self):
        self._checked_i -= 1
        if self._checked_i < 0: self._checked_i = len(self.buttons) - 1
        self.buttons[self._checked_i].set_checked(True)
