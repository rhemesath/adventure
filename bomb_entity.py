from typing import Optional
from random import uniform as rand
from math import sqrt

import shared
from entity import Entity, State, KnockbackState
from images import img_shadow_small, ani_explosion
from items import BOMB
from frame import Frame
from animation import Animation
from explosion_particle import ExplosionParticle, FireExplosionParticle, ShockwaveParticle
from dust_particle import DustParticleSpawner
from constants import HIT, STATE_IDLE


class BombDamageEntity(Entity):
    def __init__(self, bomb: "BombEntity"):
        super().__init__(0, 0, bomb.radius * 16, bomb.radius * 16)
        self.cx, self.cy = bomb.center
        self.center = (self.cx, self.cy)
        self.bomb = bomb

        self.check_collisions = True

        self.wait = 1

    def check_health(self):
        if self.wait > 0:
            self.wait -= 1
            return
        self.die()

    def on_collision(self, entity: "Entity"):
        if entity is self.bomb: return
        damage = self.bomb.get_damage_by_distance(
            entity.center_x - self.cx,
            entity.center_y - self.cy
        )
        if damage > 0:
            entity.take_damage(self.bomb, damage)

    def move(self): pass
    def render(self): pass


class IdleState(State):
    name = STATE_IDLE

    def move_x(self):
        self._user.xf += self._user._vel_x
        self._user._vel_x *= self._user._acc

    def move_y(self):
        self._user.yf += self._user._vel_y
        self._user._vel_y *= self._user._acc


class BombEntity(Entity):
    def __init__(self, cx: int, cy: int, cross_dir: int):
        super().__init__(0, 0, 4, 4, shadow=img_shadow_small)
        self.center = (cx, cy)

        self.radius = 3
        self.min_damage = 12
        self.max_damage = 60

        self.z = 16

        vel = 2.5
        dir_ = 1j ** cross_dir
        self._vel_x = dir_.real * vel
        self._vel_y = dir_.imag * vel
        self._vel_z = 1.6
        self._gravity = -0.2
        self._acc = 0.95
        self._bounciness = 0.6

        self._img_align = self.align_cx_b
        self._frame = BOMB.gfx

        self.check_collisions = True
        self.idle_state = IdleState(self)
        self.knockback_state = KnockbackState(self)

        self.light_radius = 16

        self._frame_counter = 0
        self._wait_frames = 60 * 2
        self._warn_frames = 60 * 1.5
        self._close_frames = ani_explosion.length
        self._ani = Animation((Frame(0.5,10, 8,8), BOMB.gfx), 4, loop=1)

        self.change_state(self.idle_state)

    def get_damage_by_distance(self, dx: float, dy: float) -> int:
        dist = sqrt(dx * dx + dy * dy) / 16
        if dist > self.radius: return 0
        return round((1 - (dist / self.radius)) * (self.max_damage - self.min_damage) + self.min_damage)

    def update(self):
        self._state.update()

    def check_health(self):
        if self._warn_frames == 0:
            self.explode()

    def take_damage(self, from_entity: Optional["Entity"], damage: int) -> int:
        if from_entity is not None:
            self.change_state(self.knockback_state)
            self.knockback_state.set_knockback(from_entity)
        return HIT

    def set_frame(self):
        if self._wait_frames > 0:
            self._wait_frames -= 1
            if self._wait_frames == 0: shared.sound.play("bomb_fizzle")
        elif self._warn_frames > 0:
            self._warn_frames -= 1
            self._frame = self._ani.get_frame(self._frame_counter)
            self._frame_counter += 1

            if self._warn_frames == self._close_frames:
                shared.game.ent_man.add(ExplosionParticle(self.center_x, self.center_y))

            if self._warn_frames <= 1:
                self.sort_off = 10_000
                self.light_radius = 800

    def explode(self):
        shared.game.ent_man.add(BombDamageEntity(self))

        # hit tiles
        hit_tile = shared.game.world.hit_tile
        cx, cy = self.center_x, self.center_y
        tile_x, tile_y = cx // 16, cy // 16
        for ty in range(tile_y - self.radius, tile_y + self.radius +1):
            dy = ty * 16 + 8 - cy
            for tx in range(tile_x - self.radius, tile_x + self.radius +1):
                dx = tx * 16 + 8 - cx
                damage = self.get_damage_by_distance(dx, dy)
                if damage == 0: continue
                hit_tile(tx, ty, damage=damage)

        for _ in range(5):
            dist = (1j ** rand(0, 4)) * rand(0, self.radius * 16 / 4)
            shared.game.ent_man.add(DustParticleSpawner(cx + dist.real, cy + dist.imag))

        for _ in range(15):
            dist = (1j ** rand(0, 4)) * rand(0, self.radius * 16 / 2)
            shared.game.ent_man.add(FireExplosionParticle(cx + dist.real, cy + dist.imag))

        for _ in range(8):
            shared.game.ent_man.add(ShockwaveParticle(cx, cy))

        shared.sound.play("explosion")
        self.die()

    def move(self):
        self._state.move()
        super().move()

    def _move_x(self): self._state.move_x()
    def _move_y(self): self._state.move_y()

    def render(self):
        super().render()
        if self._warn_frames <= 1:
            shared.paint.rect((255, 255, 255, 100), *shared.game.cam.get_rect()) # FIXME
