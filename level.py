from typing import List, Optional
from abc import ABC, abstractmethod
import json

from age import Texture

import shared
from tile import Tile, TileType
from tiles import predefined_tiles, GRASS, CAVE_FLOOR
from constants import LEVELS_DIR
from light_source import LightSource
from water_droplet import WaterDroplet


class Level(ABC):
    def __init__(self, name: str, default_tile: Tile, music:Optional[str]=None):
        self.name = name
        self.default_tile = default_tile
        self.music = music
        self.darkness = 0
        self.floor_map: List[List[Tile]]
        self.tile_map: List[List[Optional[Tile]]]
        self.map_texture: Texture # mini map
        self.loaded = False

    @abstractmethod
    def load(self):
        pass

    def save(self):
        def encode(tile: Optional[Tile]) -> str:
            if tile is None: return "0:0"
            if tile.id not in predefined_tiles: return f"{tile.id}:0"
            return f"{tile.id}:{tile.type_.id}"

        def rle_encode(tiles: List[str]) -> str:
            data = []
            prev_tile = tiles[0]
            count = 0
            for tile in tiles + [""]:
                if tile == prev_tile: count += 1
                else:
                    data.append(f"{count}x{prev_tile}")
                    count = 1
                prev_tile = tile
            return ";".join(data)

        w, h = len(self.tile_map[0]), len(self.tile_map)
        data = {
            "w": w, "h": h,
            "floor": rle_encode([encode(tile) for line in self.floor_map for tile in line]),
            "tiles": rle_encode([encode(tile) for line in self.tile_map  for tile in line])
        }

        with LEVELS_DIR.joinpath(f"{self.name}.json").open("w") as level_file:
            json.dump(data, level_file)

    def _load_file(self):
        def decode(tile_data: str) -> Optional[Tile]:
            tile_id_str, tile_type_id = tile_data.split(":")
            if tile_id_str == "0": return None
            return Tile.new(int(tile_id_str), TileType.all_.get(tile_type_id))

        def rle_decode(data_str: str) -> List[str]:
            data: List[str] = []
            for rle_data in data_str.split(";"):
                count_str, tile_str = rle_data.split("x", 1)
                data += [tile_str] * int(count_str)
            return data

        with LEVELS_DIR.joinpath(f"{self.name}.json").open() as level_file:
            data = json.load(level_file)

        w, h = data["w"], data["h"]
        #floor_data = data["floor"].split(";") #rle_decode(data["floor"])
        #tiles_data = data["tiles"].split(";") #rle_decode(data["tiles"])
        floor_data = rle_decode(data["floor"])
        tiles_data = rle_decode(data["tiles"])
        self.floor_map = [[decode(floor_data[x + y * w]) for x in range(w)] for y in range(h)]
        self.tile_map  = [[decode(tiles_data[x + y * w]) for x in range(w)] for y in range(h)]

        self._setup_map_texture(w, h)

    def spawn(self):
        if shared.game.player is not None: shared.game.player.spawn()

    def _new(self):
        w, h = shared.game.world.tile_w, shared.game.world.tile_h

        self.floor_map = [[self.default_tile for _ in range(w)] for _ in range(h)]
        self.tile_map  = [[None for _ in range(w)] for _ in range(h)]

        self._setup_map_texture(w, h)

    def _setup_map_texture(self, tile_w: int, tile_h: int):
        self.map_texture = Texture.create(tile_w, tile_h)
        with shared.paint.target_texture(self.map_texture):
            shared.paint.fill((30, 20, 0))
            shared.paint.rect((50, 30, 0), 1, 1, tile_w-2, tile_h-2)


class IslandLevel(Level):
    def __init__(self):
        super().__init__("island", GRASS, "overworld")

    def load(self):
        self._load_file()
        self.loaded = True


class CaveLevelHub(Level):
    def __init__(self):
        super().__init__("cave_hub", CAVE_FLOOR, "cave_sub1_v1")
        self.darkness = 255

    def load(self):
        self._load_file()
        self.loaded = True

    def spawn(self):
        super().spawn()

        shared.game.ent_man.add(LightSource(
            shared.game.player.tile_x * 16 + 8,
            shared.game.player.tile_y * 16 + 8,
            70
        ))

        for _ in range(3): shared.game.ent_man.add(WaterDroplet())


class CaveLevel1(Level):
    def __init__(self):
        super().__init__("cave1", CAVE_FLOOR, "cave_sub2_v1")
        self.darkness = 255

    def load(self):
        self._load_file()
        self.loaded = True

    def spawn(self):
        super().spawn()

        shared.game.ent_man.add(LightSource(
            shared.game.player.tile_x * 16 + 8,
            shared.game.player.tile_y * 16 + 8,
            70
        ))
