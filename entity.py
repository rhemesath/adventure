from typing import Optional, Callable, ClassVar
from math import sqrt

import shared
from rect import Rect
from frame import Frame
from images import img_entity_default
from constants import HIT, NOT_HIT, KILLED, STATE_KNOCKBACK


def clamp(a: int) -> int:
    return 0 if a < 128 else 255


class Entity(Rect):
    def __init__(self, x: int, y: int, w:int=16, h:int=16, shadow:Optional[Frame]=None):
        super().__init__(x, y, w, h)

        self._prev_x: float = x
        self._prev_y: float = y
        self.z: float = 0
        self._vel_z: float = 0
        self._gravity: float = 0
        self._bounciness: float = 0

        self._bound_to_world = True

        self._frame = img_entity_default
        self._img_rect = Rect(0, 0, 0, 0)
        self._img_align: Callable[[Rect, Rect], None] = Rect.align_none
        self.sort_off = 0
        self._shadow = shadow

        self.light_radius: float = 0

        self.check_collisions = False
        self.has_health = False
        self.health = 0
        self._invulnerability_frames = 0
        self.invulnerable = 0

        self._state: Optional["State"] = None

    def update(self):
        pass

    def move(self):
        self._prev_x = self.xf
        self._move_x()
        self._check_tile_col_x()

        self._prev_y = self.yf
        self._move_y()
        self._check_tile_col_y()

        if self._bound_to_world:
            self.keep_inside(shared.game.world)

        self._move_z()

    def on_collision(self, entity: "Entity"):
        pass

    def action(self):
        pass

    def check_health(self):
        if self.invulnerable > 0: self.invulnerable -= 1

    def set_frame(self):
        pass

    def die(self):
        shared.game.ent_man.remove(self)

    def take_damage(self, from_entity: Optional["Entity"], damage: int) -> int:
        if not self.has_health or self.invulnerable > 0: return NOT_HIT
        self.health -= damage
        self.invulnerable = self._invulnerability_frames
        if self.health > 0: return HIT
        return KILLED

    def render(self):
        frame = self._frame
        img = self._img_rect

        self._render_shadow()

        if self.invulnerable > 0:
            shared.main_tex.set_color_mod((255, clamp(self.invulnerable * 20 % 255), clamp(self.invulnerable * 20 % 255)))

        img.size = frame.size
        self._img_align(img, self)
        shared.paint.mod_texture(shared.main_tex,
            img.x + frame.off_x,
            img.y + frame.off_y - int(self.z),
            src=frame.get_rect(),
            flip_lr=frame.flip_lr, flip_tb=frame.flip_tb,
            angle=frame.angle
        )

        if self.invulnerable > 0:
            shared.main_tex.set_color_mod((255, 255, 255))

    def change_state(self, state: "State"):
        if self._state is not None: self._state.exit()
        state.enter()
        self._state = state

    def _render_shadow(self):
        shadow = self._shadow
        if shadow is None: return
        img = self._img_rect
        img.size = shadow.size
        Rect.align_cx_b(img, self)
        shared.paint.texture(shared.main_tex,
            img.x + shadow.off_x, img.y + shadow.off_y,
            src=shadow.get_rect()
        )

    def _move_x(self): pass
    def _move_y(self): pass
    def _move_z(self):
        if self._gravity == 0: return
        self._vel_z += self._gravity
        self.z += self._vel_z
        if self.z <= 0:
            self.z = 0
            self._vel_z = -self._vel_z * self._bounciness
            if abs(self._vel_z) < 0.01:
                self._vel_z = 0
                self._gravity = 0

    def _check_tile_col_x(self):
        dx = self.xf - self._prev_x
        if dx == 0: return

        get_tile = shared.game.world.get_real_tile
        tile_x = self.tile_x if dx < 0 else self.tile_r
        for tile_y in range(self.tile_y, self.tile_b + 1):
            if not get_tile(tile_x, tile_y).type_.solid: continue
            if dx < 0: self.tile_x = tile_x + 1
            else: self.tile_r = tile_x - 1
            self._on_tile_col_x(tile_x, tile_y, dx)
            break

    def _check_tile_col_y(self):
        dy = self.yf - self._prev_y
        if dy == 0: return

        get_tile = shared.game.world.get_real_tile
        tile_y = self.tile_y if dy < 0 else self.tile_b
        for tile_x in range(self.tile_x, self.tile_r + 1):
            if not get_tile(tile_x, tile_y).type_.solid: continue
            if dy < 0: self.tile_y = tile_y + 1
            else: self.tile_b = tile_y - 1
            self._on_tile_col_y(tile_x, tile_y, dy)
            break

    def _on_tile_col_x(self, tile_x: int, tile_y: int, dx: float): pass
    def _on_tile_col_y(self, tile_x: int, tile_y: int, dy: float): pass


class State:
    name: ClassVar[int]

    def __init__(self, user: Entity):
        self._user = user

    def enter(self): pass
    def exit(self): pass

    def update(self): pass

    def move(self): pass
    def move_x(self): pass
    def move_y(self): pass

    def action(self): pass

    def get_frame(self) -> Frame: pass

    def render(self): pass


class KnockbackState(State):
    name = STATE_KNOCKBACK

    def __init__(self, user: Entity):
        super().__init__(user)
        self._acc = 0.9
        self._vel = 0.0
        self._dir_x = 0.0
        self._dir_y = 0.0

    def set_knockback(self, entity: Entity):
        dx = self._user.center_x - entity.center_x
        dy = self._user.center_y - entity.center_y
        if dx == dy == 0.0: norm_fac = 0
        else: norm_fac = 1 / sqrt(dx * dx + dy * dy) # normalize
        self._dir_x = dx * norm_fac
        self._dir_y = dy * norm_fac

        self._vel = 4.0

    def update(self):
        if self._vel <= 0.8:
            self._user.change_state(self._user.idle_state)
            return

    def move(self):
        self._vel *= self._acc

    def move_x(self):
        self._user.xf += self._dir_x * self._vel
    def move_y(self):
        self._user.yf += self._dir_y * self._vel
