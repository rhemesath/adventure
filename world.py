from typing import List, Dict, Optional

from age import Texture, Painter

import shared
from rect import Rect
from tile import Tile
from item_entity import ItemEntity
from damage_particle import DamageParticle
from tile_particle import TileParticle
from level import Level, IslandLevel, CaveLevelHub, CaveLevel1
from constants import TILE_PASSED, TILE_NOT_HIT, TILE_HIT, TILE_DESTROYED
from images import ani_map_pointer


class World(Rect):
    def __init__(self):
        super().__init__(0, 0, 0, 0)

        self._floor_map: List[List[Tile]]
        self._tile_map: List[List[Optional[Tile]]] = []
        self.tile_w = 120
        self.tile_h = 120
        self.w = self.tile_w * 16
        self.h = self.tile_h * 16

        self.default_tile: Optional[Tile] = None
        self.darkness = 0

        self._tile_healths: Dict[int, int] = {}

        self._level_i: int = -1
        self._levels: List[Level] = [
            IslandLevel(), CaveLevelHub(), CaveLevel1()
        ]

        self._minimap_tex: Texture

    def save(self):
        self._levels[self._level_i].save()

    def change_level(self, delta_level: int):
        self.set_level(self._level_i + delta_level)

    def set_level(self, level_i: int):
        if level_i == self._level_i: return
        self._level_i = level_i

        level = self._levels[level_i]
        self.darkness = level.darkness
        shared.game.music.play(level.music)
        if not level.loaded: level.load()
        self._minimap_tex = level.map_texture

        shared.age.audio.remove_all_effects()
        if level_i > 0:
            shared.age.audio.add_audio_effect(shared.cave_audio_effect)

        self._floor_map = level.floor_map
        self._tile_map = level.tile_map
        self.default_tile = level.default_tile
        self._tile_healths.clear()

        shared.game.ent_man.clear()
        level.spawn()

    def hit_tile(self, tile_x: int, tile_y: int, damage:int=1) -> int:
        tile = self.get_tile(tile_x, tile_y)
        if tile is None: return TILE_PASSED
        type_ = tile.type_
        if type_.health <= 0: return TILE_PASSED
        if damage < type_.min_damage:
            shared.game.ent_man.add(DamageParticle(0, tile_x*16+8, tile_y*16-4))
            return TILE_NOT_HIT

        hash_ = self.tile_hash(tile_x, tile_y)
        health = self._tile_healths.get(hash_, type_.health) - damage
        if health <= 0:
            self.set_tile_(type_.get(damage), tile_x, tile_y)
            if type_.item is not None:
                shared.game.ent_man.add(ItemEntity(type_.item, tile_x*16+8, tile_y*16+8))
            if type_.particle is not None:
                for _ in range(3): shared.game.ent_man.add(TileParticle(type_.particle, tile_x, tile_y))
            shared.game.cam.shake(1.4, 0.8)
        else:
            self._tile_healths[hash_] = health
            if type_.particle is not None:
                shared.game.ent_man.add(TileParticle(type_.particle, tile_x, tile_y))
        shared.game.ent_man.add(DamageParticle(damage, tile_x*16+8, tile_y*16-4))

        return TILE_DESTROYED if health <= 0 else TILE_HIT

    def get_real_tile(self, tile_x: int, tile_y: int) -> Tile:
        if not self.tile_exists(tile_x, tile_y): return self.default_tile
        return self.get_tile_(tile_x, tile_y) or self.get_floor_tile_(tile_x, tile_y)

    def get_tile(self, tile_x: int, tile_y: int) -> Optional[Tile]:
        if not self.tile_exists(tile_x, tile_y): return None
        return self.get_tile_(tile_x, tile_y)

    def get_floor_tile(self, tile_x: int, tile_y: int) -> Tile:
        if not self.tile_exists(tile_x, tile_y): return self.default_tile
        return self.get_floor_tile_(tile_x, tile_y)

    def set_tile(self, tile: Optional[Tile], tile_x: int, tile_y: int):
        if not self.tile_exists(tile_x, tile_y): return
        self.set_tile_(tile, tile_x, tile_y)

    def set_floor_tile(self, tile: Optional[Tile], tile_x: int, tile_y: int):
        if not self.tile_exists(tile_x, tile_y): return
        self.set_floor_tile_(tile, tile_x, tile_y)

    def get_tile_(self, tile_x: int, tile_y: int) -> Optional[Tile]:
        return self._tile_map[tile_y][tile_x]

    def set_tile_(self, tile: Optional[Tile], tile_x: int, tile_y: int):
        self._tile_healths.pop(self.tile_hash(tile_x, tile_y), None)
        self._tile_map[tile_y][tile_x] = tile

    def get_floor_tile_(self, tile_x: int, tile_y: int) -> Tile:
        return self._floor_map[tile_y][tile_x]

    def set_floor_tile_(self, tile: Optional[Tile], tile_x: int, tile_y: int):
        if tile is None: tile = self.default_tile
        self._floor_map[tile_y][tile_x] = tile

    def tile_exists(self, tile_x: int, tile_y: int) -> bool:
        return 0 <= tile_x < self.tile_w and 0 <= tile_y < self.tile_h

    def render(self):
        self.render_(16)

        # minimap
        cam_pos = shared.paint.get_cam_pos()
        shared.paint.set_cam_pos(0, 0)
        with shared.paint.target_texture(self._minimap_tex):
            self.render_(1)
        shared.paint.set_cam_pos(*cam_pos)

    def render_(self, tilesize: int):
        tex = shared.main_tex
        paint_texture = shared.paint.texture
        cam = shared.game.cam
        tex_w = tex.w // 16

        range_x = range(cam.tile_x, cam.tile_ri + 1)
        for tile_y in range(cam.tile_y, cam.tile_bi + 1):
            if tile_y < 0: continue
            elif tile_y >= self.tile_h: break
            floor_row = self._floor_map[tile_y]
            row = self._tile_map[tile_y]
            y = tile_y * tilesize
            for tile_x in range_x:
                if tile_x < 0: continue
                elif tile_x >= self.tile_w: break
                gfx = floor_row[tile_x].gfx
                paint_texture(tex, tile_x * tilesize, y, w=tilesize, h=tilesize, src=(
                    (gfx %  tex_w) * 16,
                    (gfx // tex_w) * 16,
                    16, 16
                ))
                if row[tile_x] is None: continue
                gfx = row[tile_x].gfx
                paint_texture(tex, tile_x * tilesize, y, w=tilesize, h=tilesize, src=(
                    (gfx %  tex_w) * 16,
                    (gfx // tex_w) * 16,
                    16, 16
                ))

    def render_ground_(self, tilesize: int, tex: Texture, paint_texture: Painter.texture, cam: Rect):
        tex_w = tex.w // 16

        range_x = range(cam.tile_x, cam.tile_ri + 1)
        for tile_y in range(cam.tile_y, cam.tile_bi + 1):
            if tile_y < 0: continue
            elif tile_y >= self.tile_h: break
            floor_row = self._floor_map[tile_y]
            row = self._tile_map[tile_y]
            y = tile_y * tilesize
            for tile_x in range_x:
                if tile_x < 0: continue
                elif tile_x >= self.tile_w: break
                gfx = floor_row[tile_x].gfx
                paint_texture(tex, tile_x * tilesize, y, w=tilesize, h=tilesize, src=(
                    (gfx %  tex_w) * 16,
                    (gfx // tex_w) * 16,
                    16, 16
                ))

    def render_layer_(self, tilesize: int, tex: Texture, paint_texture: Painter.texture, cam: Rect):
        tex_w = tex.w // 16

        range_x = range(cam.tile_x, cam.tile_ri + 1)
        for tile_y in range(cam.tile_y, cam.tile_bi + 1):
            if tile_y < 0: continue
            elif tile_y >= self.tile_h: break
            row = self._tile_map[tile_y]
            y = tile_y * tilesize
            for tile_x in range_x:
                if tile_x < 0: continue
                elif tile_x >= self.tile_w: break
                if row[tile_x] is None: continue
                gfx = row[tile_x].gfx
                paint_texture(tex, tile_x * tilesize, y, w=tilesize, h=tilesize, src=(
                    (gfx %  tex_w) * 16,
                    (gfx // tex_w) * 16,
                    16, 16
                ))

    def render_minimap(self):
        shared.paint.rect((0, 0, 0, 200), 0, 0, shared.game.w, shared.game.h)
        off_x = (shared.game.w - self._minimap_tex.w) // 2
        off_y = (shared.game.h - self._minimap_tex.h) // 2
        shared.paint.texture(self._minimap_tex, off_x, off_y)
        
        pointer = ani_map_pointer.get_frame(shared.game.frame_counter)
        shared.paint.texture(shared.main_tex,
            shared.game.player.tile_cx + off_x + pointer.off_x,
            shared.game.player.tile_cy + off_y + pointer.off_y,
            src=pointer.get_rect()
        )

    def tile_hash(self, tile_x: int, tile_y: int) -> int:
        return tile_y * self.tile_w + tile_x
