from typing import Optional, Callable

from frame import Frame


class Item:
    def __init__(self,
            name: str,
            gfx_tile_x: float, gfx_tile_y: float,
            damage:int=1,
            stackable:bool=False,
            use:Optional[Callable[[int, int], bool]]=None,
            get:Optional[Callable[[], None]]=None
        ):
        self.name = name
        self.gfx = Frame(gfx_tile_x, gfx_tile_y, 8, 8)
        self.damage = damage
        self.stackable = stackable
        self.use = use
        self.get = get
