from rect import Rect


class Frame(Rect):
    def __init__(self,
        tile_x: float, tile_y: float,
        w:int=16, h:int=16,
        off_x:int=0, off_y:int=0,
        flip_lr:int=0, flip_tb:int=0,
        angle:float=0.0
    ):
        super().__init__(int(tile_x * 16), int(tile_y * 16), w, h)
        self.off_x = off_x
        self.off_y = off_y
        self.flip_lr = bool(flip_lr)
        self.flip_tb = bool(flip_tb)
        self.angle = angle

    def set(self, tile_x: float, tile_y: float):
        self.x = int(tile_x * 16)
        self.y = int(tile_y * 16)
