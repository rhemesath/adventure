import shared
from entity import Entity
from frame import Frame
from item import Item
from constants import (
    DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN,
    TILE_HIT, TILE_NOT_HIT, TILE_DESTROYED, TILE_PASSED,
    HIT
)


class AttackRect(Entity):
    def __init__(self, user: Entity):
        super().__init__(0, 0)

        self._user = user
        self._item: Item

        self.sort_off = 1000
        self.check_collisions = True

        self._img = Frame(0,0, 8,8)
        self._swoosh_frame = Frame(7, 11)
        self._delta_angle: float = 0
        self._start_angle: float = 0
        self._angle_dir: int = 0
        self._angle_range = 120

        self._does_damage = False

        self._active = False

    def use(self, item: Item, cross_dir: int):
        self._active = True
        self._does_damage = True
        self._item = item
        self.align(self._user, cross_dir)

        gfx = self._item.gfx
        self._img.x, self._img.y = gfx.x, gfx.y

        flip_lr = 0
        delta_angle = self._angle_range / 60 / 0.1
        if cross_dir == DIR_DOWN:
            self._img.angle = -90
            self._angle_dir = -1
            flip_lr = 1
        elif cross_dir == DIR_UP:
            self._img.angle = 90
            self._angle_dir = -1
            flip_lr = 1
        elif cross_dir == DIR_RIGHT:
            self._img.angle = 0
            self._angle_dir = 1
        elif cross_dir == DIR_LEFT:
            self._img.angle = 0
            self._angle_dir = -1
            flip_lr = 1

        self._start_angle = self._img.angle
        self._delta_angle = delta_angle * self._angle_dir

        self._img.flip_lr = flip_lr
        self._swoosh_frame.flip_lr = flip_lr

    def move(self): pass

    def on_collision(self, entity: "Entity"):
        if not self._does_damage or entity is shared.game.player: return
        if entity.take_damage(self._user, self._item.damage) == HIT:
            shared.sound.play("entity_hit")
            self._does_damage = False

    def action(self):
        if not self._does_damage: return

        state = shared.game.world.hit_tile(
            self.center_x // 16, self.center_y // 16,
            damage=self._item.damage
        )
        if   state == TILE_NOT_HIT:   shared.sound.play("clink")
        elif state == TILE_HIT:       shared.sound.play("tile")
        elif state == TILE_DESTROYED: shared.sound.play("tile_break")
        elif state == TILE_PASSED:    shared.sound.play("swoosh")

        self._does_damage = False

    def set_frame(self):
        if not self._active: return

        angle = self._img.angle
        if (
            (self._angle_dir > 0 and angle >= self._start_angle + self._angle_range) or
            (self._angle_dir < 0 and angle <= self._start_angle - self._angle_range)
        ):
            self._active = False
            return

        angle += self._delta_angle

        rot = 1j**(angle / 90 - 1) * 17
        off_x, off_y = int(rot.real), int(rot.imag)

        for frame in (self._img, self._swoosh_frame):
            frame.off_x, frame.off_y = off_x, off_y
            frame.angle = angle

    def render(self):
        if not self._active: return
        self._frame = self._swoosh_frame
        self._render()
        self._frame = self._img
        self._render()

    def _render(self):
        frame = self._frame
        img_rect = self._img_rect

        img_rect.size = frame.size
        img_rect.center = self._user.center

        shared.paint.mod_texture(shared.main_tex,
            int(img_rect.x) + frame.off_x,
            int(img_rect.y) + frame.off_y,
            src=frame.get_rect(),
            flip_lr=frame.flip_lr, flip_tb=frame.flip_tb,
            angle=frame.angle
        )


class UseActionRect(Entity):
    def __init__(self, user: Entity):
        super().__init__(0, 0)

        self._user = user
        self._item: Item

        self._img = Frame(9, 11)
        self.sort_off = 1000
        self._img_align = self.align_c

        self._active = False

    def use(self, item: Item, cross_dir: int):
        self._active = True
        self._item = item
        self.align(self._user, cross_dir)

    def action(self):
        if not self._active: return

        tile_x, tile_y = self.center_x // 16, self.center_y // 16
        tile = shared.game.world.get_real_tile(tile_x, tile_y)
        if tile.type_.interact is not None:
            if tile.type_.interact(tile_x, tile_y):
                pass # TODO effect?

        elif self._item.use is not None:
            if self._item.use(self.center_x, self.center_y):
                shared.game.inventory.remove(self._item)

    def move(self): pass

    def render(self):
        if not self._active: return
        self._frame = self._img
        super().render()
        if self._item.use is not None:
            self._frame = self._frame = self._item.gfx
            super().render()
        self._active = False
