from random import uniform as rand

import shared
from entity import Entity
from images import ani_dust_particle, img_shadow_small


class DustParticle(Entity):
    def __init__(self, cx: int, cy: int, angle: float, vel: float, vel_z: float, frame: int, shadow: bool):
        """ angle: 0.0 - 1.0 """
        super().__init__(0, 0, 8, 8, shadow=img_shadow_small if shadow else None)
        self.center = (cx, cy)
        self.z = 1

        dir_ = 1j ** (angle * 4)
        self._vel_x =  dir_.real * vel
        self._vel_y =  dir_.imag * vel
        self._vel_z = vel_z
        self._acc = 0.98
        self._gravity = -0.05
        self._bounciness = 0#0.2

        if shadow: self.light_radius = 16

        self._bound_to_world = False

        self._frame = ani_dust_particle.get_frame(frame)

    def check_health(self):
        if self.z <= 0.1 and abs(self._vel_z) <= 0.01:
            self.die()

    def set_frame(self):
        if self.light_radius > 0: self.light_radius *= 0.99

    def _move_x(self):
        self.xf += self._vel_x
        self._vel_x *= self._acc

    def _move_y(self):
        self.yf += self._vel_y
        self._vel_y *= self._acc

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass


class DustParticleSpawner(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(cx, cy, 1, 1)

        self.count = 0

        self._angle = rand(0, 1)
        self._vel = rand(0.4, 1.6)
        self._vel_z = rand(0.5, 2.4)

    def move(self): pass

    def action(self):
        shared.game.ent_man.add(DustParticle(
            self.x, self.y, self._angle, self._vel, self._vel_z, self.count,
            self.count == 0 # only set shadow for the first particle
        ))
        self.count += 1

    def check_health(self):
        if self.count == ani_dust_particle.length:
            self.die()

    def render(self): pass
