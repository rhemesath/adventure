from typing import Callable, Optional, Tuple
from dataclasses import dataclass, replace


@dataclass
class Settings:
    canvas_res:  Tuple[int, int] = (800, 600)
    window_size: Tuple[int, int] = None
    title: str = "Window"
    window_resizable: bool = False
    pixel_perfect: bool = True
    main_window_quits: bool = True # quits AGE if the main Window is closed
    vsync: bool = True # enables VSync for the very first window

    audio_channels: int    = 2 # 1 for Mono, 2 for Stereo
    audio_frequency: int   = 44100 # CD quality
    audio_buffer_size: int = 1

    use_gamepads: bool = False

    hide_warnings: bool = False

    def copy(self) -> "Settings": return replace(self)
