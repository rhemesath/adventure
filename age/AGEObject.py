from typing import ClassVar

from .Settings import Settings


class AGEObject:
    _age: ClassVar["AGE"]
    _s:   ClassVar[Settings]

    @classmethod
    def setup_age_object_(cls, age: "AGE", settings: Settings):
        cls._age = age
        cls._s = settings

    def quit_(self):
        pass


# import is here because of circular import
from . import AGE
