from typing import Optional, List, Dict
from ctypes import POINTER
from math import atan2, radians, sin, cos

from sdl2 import (
    SDL_GameControllerOpen, SDL_GameControllerClose,
    SDL_IsGameController, SDL_GameControllerMapping,
    SDL_GameControllerButton, SDL_GameControllerGetButton,
    SDL_GameControllerAxis, SDL_GameControllerGetAxis,
    SDL_GameControllerGetJoystick, SDL_JoystickInstanceID,
    SDL_Joystick, SDL_GameControllerRumble,
    SDL_CONTROLLER_AXIS_LEFTX, SDL_CONTROLLER_AXIS_LEFTY,
    SDL_CONTROLLER_BUTTON_DPAD_LEFT, SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
    SDL_CONTROLLER_BUTTON_DPAD_UP, SDL_CONTROLLER_BUTTON_DPAD_DOWN,
)
try:
    from sdl2 import SDL_GameControllerHasRumble
except:
    from sdl2 import _bind, SDL_bool, SDL_GameController, SDL_TRUE
    try:
        SDL_GameControllerHasRumble = _bind("SDL_GameControllerHasRumble", [POINTER(SDL_GameController)], SDL_bool)
    except:
        SDL_GameControllerHasRumble = lambda _gcon: SDL_TRUE

import sdl2.gamecontroller as controller_constants

from .AGEObject import AGEObject
from .types import SDLGameController
from .exceptions import AGEException


ANALOG_DPAD_LIMIT = cos(radians(67.5))
BITS_16 = 2 ** 16 - 1


class Gamepad(AGEObject):
    def __init__(self, index: int):
        self.name = ""
        self.id: int = -1
        self._buttons: Dict[SDL_GameControllerButton, bool] = {}
        self._dpad = {
            SDL_CONTROLLER_BUTTON_DPAD_LEFT:  False,
            SDL_CONTROLLER_BUTTON_DPAD_RIGHT: False,
            SDL_CONTROLLER_BUTTON_DPAD_UP:    False,
            SDL_CONTROLLER_BUTTON_DPAD_DOWN:  False
        }
        self.button_down_events: List[SDL_GameControllerButton] = []
        self.button_up_events:   List[SDL_GameControllerButton] = []
        self.axis_motion_events: Dict[SDL_GameControllerAxis, float] = {}

        self._gcon: SDLGameController = None

        self._stick_as_dpad = False
        self._dpad_threshold_sq = 0.0

        self._has_rumble = False
        self._use_rumble = True

        self._init_sdl(index)

        self._age.gamepads.register_(self)

    def set_stick_as_dpad(self, threshold:float=0.3):
        self._stick_as_dpad = True
        self._dpad_threshold_sq = threshold * threshold

    def disable_stick_as_dpad(self):
        self._stick_as_dpad = False

    def rumble(self, intensity_left: float, intensity_right: float, duration: float):
        if not self._use_rumble or not self._has_rumble: return
        if SDL_GameControllerRumble(self._gcon,
            int(intensity_left * BITS_16), int(intensity_right * BITS_16),
            int(duration * 1000)
        ) < 0:
            self._age.check_sdl_error_("SDL_GameControllerRumble", ignore=True)

    def use_rumble(self, use: bool):
        self._use_rumble = use

    def button(self, button: str) -> bool:
        return self.button_(self.button_str_to_sdl_int_(button))

    def axis(self, axis: str) -> float:
        return self.axis_(self.axis_str_to_sdl_int_(axis))

    def button_down_event(self, button: str) -> bool:
        return self.button_str_to_sdl_int_(button) in self.button_down_events

    def button_up_event(self, button: str) -> bool:
        return self.button_str_to_sdl_int_(button) in self.button_up_events

    def axis_motion_event(self, axis: str) -> Optional[float]:
        return self.axis_motion_events.get(self.axis_str_to_sdl_int_(axis))

    def button_(self, button: SDL_GameControllerButton) -> bool:
        value = self._buttons.get(button)
        if value is None:
            value = self._buttons[button] = bool(SDL_GameControllerGetButton(self._gcon, button))
        return value

    def axis_(self, axis: SDL_GameControllerAxis) -> float:
        return SDL_GameControllerGetAxis(self._gcon, axis) / 32768

    def button_str_to_sdl_int_(self, button: str) -> SDL_GameControllerButton:
        return getattr(controller_constants, f"SDL_CONTROLLER_BUTTON_{button.upper()}")

    def axis_str_to_sdl_int_(self, axis: str) -> SDL_GameControllerAxis:
        return getattr(controller_constants, f"SDL_CONTROLLER_AXIS_{axis.upper()}")

    def handle_button_event_(self, button: SDL_GameControllerButton, down: bool):
        if down: self.button_down_events.append(button)
        else: self.button_up_events.append(button)

    def handle_axis_motion_event_(self, axis: SDL_GameControllerAxis, value: int):
        self.axis_motion_events[axis] = value / 32768

    def flush_(self):
        self._buttons.clear()
        self.button_down_events.clear()
        self.button_up_events.clear()
        self.axis_motion_events.clear()

    def update_(self):
        if not self._stick_as_dpad: return
        dpad = {button: self.button_(button) for button in self._dpad}

        if self._stick_as_dpad:
            stick_dpad = self._analog_to_dpad(
                self.axis_(SDL_CONTROLLER_AXIS_LEFTX), self.axis_(SDL_CONTROLLER_AXIS_LEFTY)
            )
            for button, state in stick_dpad.items():
                dpad[button] |= state

        for button, cur_state in dpad.items():
            prev_state = self._dpad[button]
            # emit events
            if cur_state and not prev_state: self.handle_button_event_(button, True)
            elif prev_state and not cur_state: self.handle_button_event_(button, False)

        self._dpad = dpad
        self._buttons.update(self._dpad)

    def _analog_to_dpad(self, x: float, y: float) -> Dict[SDL_GameControllerButton, bool]:
        dpad = {button: False for button in self._dpad}
        if (x * x + y * y) < self._dpad_threshold_sq: return dpad

        angle = atan2(y, x)
        x, y = cos(angle), sin(angle)

        if   x >  ANALOG_DPAD_LIMIT: dpad[SDL_CONTROLLER_BUTTON_DPAD_RIGHT] = True
        elif x < -ANALOG_DPAD_LIMIT: dpad[SDL_CONTROLLER_BUTTON_DPAD_LEFT]  = True
        if   y >  ANALOG_DPAD_LIMIT: dpad[SDL_CONTROLLER_BUTTON_DPAD_DOWN]  = True
        elif y < -ANALOG_DPAD_LIMIT: dpad[SDL_CONTROLLER_BUTTON_DPAD_UP]    = True

        return dpad

    def _init_sdl(self, index: int):
        if not SDL_IsGameController(index):
            raise AGEException(f"Device index {index} is not a gamepad")

        self._gcon = SDL_GameControllerOpen(index)
        if self._gcon is None:
            self._age.check_sdl_error_("SDL_GameControllerOpen", fatal=True)

        self.name = self._age.gamepads.get_name(index)

        mapping: Optional[bytes] = SDL_GameControllerMapping(self._gcon)
        if not mapping:
            self._age.check_sdl_error_("SDL_GameControllerMapping", fatal=True)

        self._has_rumble = bool(SDL_GameControllerHasRumble(self._gcon))

        joystick: Optional[POINTER(SDL_Joystick)] = SDL_GameControllerGetJoystick(self._gcon)
        if joystick is None:
            self._age.check_sdl_error_("SDL_GameControllerGetJoystick", fatal=True)

        joy_id: int = SDL_JoystickInstanceID(joystick)
        if joy_id < 0:
            self._age.check_sdl_error_("SDL_JoystickInstanceID", fatal=True)

        self.id = joy_id

    def quit_(self):
        if self._gcon is not None:
            SDL_GameControllerClose(self._gcon)
        self._age.gamepads.unregister_(self)
