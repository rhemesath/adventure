from typing import ClassVar, Tuple


class Camera:
    _cam_x: ClassVar[int] = 0
    _cam_y: ClassVar[int] = 0

    @classmethod
    def get_cam_pos(cls) -> Tuple[int]: return (cls._cam_x, cls._cam_y)
    @classmethod
    def get_cam_x(cls) -> int: return cls._cam_x
    @classmethod
    def get_cam_y(cls) -> int: return cls._cam_y

    @classmethod
    def set_cam_pos(cls, x: int, y: int): cls._cam_x, cls._cam_y = x, y
    @classmethod
    def set_cam_x(cls, x: int): cls._cam_x = x
    @classmethod
    def set_cam_y(cls, y: int): cls._cam_y = y
