from typing import ClassVar, Dict, List, Tuple
from ctypes import POINTER, c_ubyte

from sdl2 import (
    SDL_KeyboardEvent, SDL_GetKeyboardState, SDL_KEYDOWN,
    SDL_KEYUP, SDL_EventType
)
import sdl2.scancode as sdl_scancodes


class Keyboard:
    event_types: ClassVar[Tuple[SDL_EventType]] = (SDL_KEYDOWN, SDL_KEYUP)
    _global_state: ClassVar[POINTER(c_ubyte)] = None

    @classmethod
    def setup_(cls):
        cls._global_state = SDL_GetKeyboardState(None)

    @classmethod
    def get_state(cls, key: str) -> bool:
        return cls.get_state_(cls.key_str_as_sdl_scancode_(key))

    def __init__(self):
        self._pressed: Dict[int, bool] = {}
        self._down_events: List[int] = []
        self._up_events: List[int] = []

    def is_pressed(self, key: str) -> bool:
        return self.is_pressed_(self.key_str_as_sdl_scancode_(key))

    def down_event(self, key: str) -> bool:
        return self.down_event_(self.key_str_as_sdl_scancode_(key))

    def up_event(self, key: str) -> bool:
        return self.up_event_(self.key_str_as_sdl_scancode_(key))

    def is_pressed_(self, sdl_scancode: int) -> bool:
        return self._pressed.get(sdl_scancode, False)

    def down_event_(self, sdl_scancode: int) -> bool:
        return sdl_scancode in self._down_events

    def up_event_(self, sdl_scancode: int) -> bool:
        return sdl_scancode in self._up_events

    def handle_sdl_event_(self, event: SDL_KeyboardEvent):
        if event.repeat: return
        scancode: int = event.keysym.scancode

        if event.type == SDL_KEYDOWN:
            self._down_events.append(scancode)
            self._pressed[scancode] = True
        elif event.type == SDL_KEYUP:
            self._up_events.append(scancode)
            self._pressed[scancode] = False

    def flush_(self):
        self._down_events.clear()
        self._up_events.clear()

    @classmethod
    def get_state_(cls, sdl_scancode: int) -> bool:
        return bool(cls._global_state[sdl_scancode])

    @staticmethod
    def key_str_as_sdl_scancode_(key: str) -> int:
        return getattr(sdl_scancodes, f"SDL_SCANCODE_{key.upper()}")
