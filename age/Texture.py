from typing import Union, Tuple, Optional
from pathlib import Path
from ctypes import c_int, byref, c_void_p, c_ubyte, cast

from sdl2 import (
    SDL_CreateTexture, SDL_DestroyTexture, SDL_PIXELFORMAT_RGBA32,
    SDL_TEXTUREACCESS_TARGET, SDL_QueryTexture, SDL_CreateTextureFromSurface,
    SDL_TEXTUREACCESS_STREAMING, SDL_SetTextureBlendMode, SDL_BLENDMODE_BLEND,
    SDL_BLENDMODE_NONE, SDL_SetTextureColorMod, SDL_RenderReadPixels,
    Uint32, SDL_UpdateTexture, SDL_GetTextureBlendMode,
    SDL_GetTextureColorMod, SDL_BlendMode, SDL_BYTESPERPIXEL,
    SDL_SetHint, SDL_HINT_RENDER_SCALE_QUALITY
)
from sdl2.sdlimage import IMG_LoadTexture

from .types import SDLTexture
from .exceptions import AGEException
from .AGEObject import AGEObject
from .Surface import Surface


class AntiAliasing:
    def __enter__(self):
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, b"1")

    def __exit__(self, *_):
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, b"0")


class Texture(AGEObject):
    tex_: SDLTexture = None
    w: int
    h: int
    streaming: bool

    def __init__(self, painter: "Painter", tex: SDLTexture, w: int, h: int, streaming:bool=False):
        self._painter = painter
        self.tex_ = tex
        self.w = w
        self.h = h
        self.streaming = streaming

        self._painter.register_texture_(self)

    @classmethod
    def create(cls, w: int, h: int, streaming:bool=False, painter:Optional["Painter"]=None) -> "Texture":
        painter = painter or cls._age.paint
        if painter is None: raise AGEException("A Painter is required")

        tex: SDLTexture = SDL_CreateTexture(
            painter.get_sdl_renderer_(),
            SDL_PIXELFORMAT_RGBA32,
            SDL_TEXTUREACCESS_STREAMING if streaming else SDL_TEXTUREACCESS_TARGET,
            w, h
        )
        if tex is None:
            cls._age.check_sdl_error_("SDL_CreateTexture", fatal=True)

        return cls(painter, tex, w, h, streaming=streaming)

    @classmethod
    def load(cls, path: Union[Path, str], painter:Optional["Painter"]=None) -> "Texture":
        painter = painter or cls._age.paint
        if painter is None: raise AGEException("A Painter is required")
        if type(path) is str: path = Path(path)

        # FIXME: Load as Surface and convert to Target Texture

        tex: SDLTexture = IMG_LoadTexture(
            painter.get_sdl_renderer_(),
            str(path).encode()
        )
        if tex is None:
            cls._age.check_sdl_error_("IMG_LoadTexture", fatal=True)

        c_w, c_h = c_int(0), c_int(0)
        if SDL_QueryTexture(tex, None, None, byref(c_w), byref(c_h)) < 0:
            cls._age.check_sdl_error_("SDL_QueryTexture", fatal=True)

        return cls(painter, tex, c_w.value, c_h.value)

    @classmethod
    def from_surface(cls, surface: Surface) -> "Texture":
        painter = surface.get_painter_()
        tex: SDLTexture = SDL_CreateTextureFromSurface(
            painter.get_sdl_renderer_(),
            surface.sf_
        )
        if tex is None:
            cls._age.check_sdl_error_("SDL_CreateTextureFromSurface", fatal=True)

        return cls(painter, tex, surface.w, surface.h)

    @classmethod
    def from_texture(cls, src_texture: "Texture", painter:Optional["Painter"]=None) -> "Texture":
        if painter is None: painter = src_texture.get_painter_()

        if painter is src_texture.get_painter_():
            return cls._from_texture_with_same_painter(src_texture)

        # query source texture
        c_w, c_h = c_int(0), c_int(0)
        src_format = Uint32(0)
        src_access = c_int(0)
        if SDL_QueryTexture(src_texture.tex_,
            byref(src_format), byref(src_access), byref(c_w), byref(c_h)
        ) < 0:
            cls._age.check_sdl_error_("SDL_QueryTexture", fatal=True)

        free_src = False
        if src_access.value != SDL_TEXTUREACCESS_TARGET:
            # create temporary copy with an access type we can actually read the from
            src_texture = cls._from_texture_with_same_painter(src_texture)
            src_format = Uint32(SDL_PIXELFORMAT_RGBA32)
            src_access = c_int(SDL_TEXTUREACCESS_TARGET)
            free_src = True

        # create sdl texture
        tex: SDLTexture = SDL_CreateTexture(painter.get_sdl_renderer_(),
            src_format.value, src_access.value,
            c_w.value, c_h.value
        )
        if tex is None:
            cls._age.check_sdl_error_("SDL_CreateTexture", fatal=True)

        # read pixels
        pitch = SDL_BYTESPERPIXEL(src_format.value) * c_w.value
        pixel_src = cast((c_ubyte * (pitch * c_h.value))(), c_void_p)
        src_painter = src_texture.get_painter_()
        with src_painter.target_texture(src_texture):
            if SDL_RenderReadPixels(src_painter.get_sdl_renderer_(),
                None, src_format.value, pixel_src, pitch
            ) < 0:
                cls._age.check_sdl_error_("SDL_RenderReadPixels", fatal=True)
        # copy pixel data
        if SDL_UpdateTexture(tex, None, pixel_src, pitch) < 0:
            cls._age.check_sdl_error_("SDL_UpdateTexture", fatal=True)

        final_tex = cls(painter, tex, src_texture.w, src_texture.h, streaming=src_texture.streaming)
        final_tex.set_color_mod(src_texture.get_color_mod())
        final_tex.set_blending(src_texture.get_blending())

        if free_src:
            src_texture.quit_()

        return final_tex

    def set_blending(self, blending: bool):
        if SDL_SetTextureBlendMode(
            self.tex_,
            SDL_BLENDMODE_BLEND if blending else SDL_BLENDMODE_NONE
        ) < 0:
            self._age.check_sdl_error_("SDL_SetTextureBlendMode")

    def get_blending(self) -> bool:
        blendmode = SDL_BlendMode()
        if SDL_GetTextureBlendMode(self.tex_, byref(blendmode)) < 0:
            self._age.check_sdl_error_("SDL_GetTextureBlendMode")
            return False
        return blendmode.value == SDL_BLENDMODE_BLEND

    def set_color_mod(self, color: Tuple[int, int, int]):
        if SDL_SetTextureColorMod(self.tex_, *color) < 0:
            self._age.check_sdl_error_("SDL_SetTextureColorMod")

    def get_color_mod(self) -> Tuple[int, int, int]:
        r, g, b = c_ubyte(0), c_ubyte(0), c_ubyte(0)
        if SDL_GetTextureColorMod(self.tex_, byref(r), byref(g), byref(b)) < 0:
            self._age.check_sdl_error_("SDL_GetTextureColorMod")
            return (255, 255, 255)
        return (r.value, g.value, b.value)

    def get_painter_(self) -> "Painter": return self._painter

    def quit_(self):
        self._painter.unregister_texture_(self)
        SDL_DestroyTexture(self.tex_)

    @classmethod
    def _from_texture_with_same_painter(cls, src_texture: "Texture") -> "Texture":
        """ created texture will be a target texture """
        painter = src_texture.get_painter_()
        dest_texture = cls.create(src_texture.w, src_texture.h, painter=painter)

        # save source state
        src_blending  = src_texture.get_blending()
        src_color_mod = src_texture.get_color_mod()
        # default source state
        src_texture.set_blending(False)
        src_texture.set_color_mod((255, 255, 255))
        # copy pixels
        with painter.target_texture(dest_texture):
            painter.texture(src_texture, 0, 0)
        # restore source state and copy it in the destination texture
        for tex in (src_texture, dest_texture):
            tex.set_blending(src_blending)
            tex.set_color_mod(src_color_mod)

        return tex

from . import Painter
