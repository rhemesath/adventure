from typing import List
from abc import ABC, abstractmethod
from ctypes import cast, POINTER, c_void_p, c_int16

from sdl2.sdlmixer import Mix_EffectFunc_t, Mix_EffectDone_t

from .AGEObject import AGEObject


sint16_ptr = POINTER(c_int16)


class AudioEffect(AGEObject, ABC):
    effect_: Mix_EffectFunc_t

    def __init__(self):
        self.effect_ = Mix_EffectFunc_t(self._effect)

    @abstractmethod
    def _effect(self, _channel: int, stream: c_void_p, length: int, _udata: c_void_p):
        pass

    @Mix_EffectDone_t
    def cleanup_(_channel: int, _udata: c_void_p):
        pass


class UserAudioEffect(AudioEffect, ABC):
    @abstractmethod
    def effect(self, data: sint16_ptr, data_len: int):
        pass

    def _effect(self, channel: int, stream: c_void_p, length: int, _udata: c_void_p):
        self.effect(cast(stream, sint16_ptr), length // 2) # length of stream in bytes -> sample is 16 bit -> divide by 2


class DelayAudioEffect(AudioEffect):
    def __init__(self, delay_secs: float):
        super().__init__()
        self._buffer: List[int] = [0 for _ in range(int(self._s.audio_frequency * delay_secs))]

    def _effect(self, _channel: int, stream: c_void_p, length: int, _udata: c_void_p):
        data = cast(stream, sint16_ptr)
        buffer = self._buffer
        for i in range(length // 2):
            data[i] = int((data[i] + buffer.pop(0)) / 2)
            buffer.append(data[i])
