from typing import Optional, ClassVar, Tuple, Dict, List
from math import sin, cos, pi
from ctypes import c_int

from sdl2 import (
    SDL_CreateRenderer, SDL_DestroyRenderer,
    SDL_RENDERER_ACCELERATED, SDL_RENDERER_PRESENTVSYNC,
    SDL_SetRenderDrawColor, SDL_RenderClear, SDL_RenderPresent,
    SDL_SetRenderDrawBlendMode, SDL_BLENDMODE_BLEND, SDL_BLENDMODE_NONE,
    SDL_Rect, SDL_RenderFillRect, SDL_RenderSetLogicalSize,
    SDL_RenderCopy, SDL_RENDERER_TARGETTEXTURE, SDL_SetRenderTarget,
    SDL_RenderSetIntegerScale, SDL_RenderCopyEx, SDL_RendererFlip,
    SDL_FLIP_NONE, SDL_FLIP_VERTICAL, SDL_FLIP_HORIZONTAL, SDL_Point,
    SDL_RenderDrawPoint, SDL_RenderGeometry, SDL_Vertex, SDL_RenderDrawLine
)
from sdl2.render import SDL_RenderSetVSync

from .types import SDLWindow, SDLRenderer, ColorType
from .AGEObject import AGEObject
from .Camera import Camera
from .Texture import Texture
from .Surface import Surface


CIRC_NGON = 14


class TargetTextureContext:
    def __init__(self, painter: "Painter",
        new_texture: Optional[Texture]
    ):
        self._paint = painter
        self._new_tex = new_texture
        self._prev_tex: Optional[Texture] = None

    def __enter__(self) -> None:
        self._prev_tex = self._paint.get_target_texture()
        self._paint.set_target_texture(self._new_tex)
        return None

    def __exit__(self, *_):
        self._paint.set_target_texture(self._prev_tex)


class Painter(AGEObject, Camera):
    _flips: ClassVar[Dict[Tuple[bool, bool], SDL_RendererFlip]] = {
        (False, False): SDL_FLIP_NONE,
        (True,  False): SDL_FLIP_HORIZONTAL,
        (False, True):  SDL_FLIP_VERTICAL,
        (True,  True):  SDL_FLIP_HORIZONTAL | SDL_FLIP_VERTICAL
    }

    _textures: ClassVar[List[Texture]] = []
    _surfaces: ClassVar[List[Surface]] = []

    _src_rect = SDL_Rect()
    _dst_rect = SDL_Rect()
    _point    = SDL_Point()

    def __init__(self, win: SDLWindow):
        self._ren: SDLRenderer = None

        self._target_texture: Optional[Texture] = None

        delta_angle = 2 * pi / CIRC_NGON
        self._circ_angles: Tuple[Tuple[float, float], ...] = tuple([
            (sin(i * delta_angle), -cos(i * delta_angle))
            for i in range(CIRC_NGON)
        ])
        self._circ_verts = (SDL_Vertex * (CIRC_NGON + 1))()
        self._circ_indices = (c_int * (CIRC_NGON * 3))()
        for i in range(CIRC_NGON):
            self._circ_indices[i * 3] = 0
            self._circ_indices[i * 3 + 1] = i + 1
            self._circ_indices[i * 3 + 2] = (i + 1) % CIRC_NGON + 1
        self._num_circ_verts = len(self._circ_verts)
        self._num_circ_indices = len(self._circ_indices)

        self._init_sdl_renderer(win)

    def fill(self, color: ColorType):
        self.set_blend_mode_(SDL_BLENDMODE_NONE)
        self.set_color_(color)
        SDL_RenderClear(self._ren)
        self.set_blend_mode_(SDL_BLENDMODE_BLEND)

    def rect(self, color: ColorType, x: int, y: int, w: int, h: int):
        d = self._dst_rect
        d.x, d.y, d.w, d.h = x - self._cam_x, y - self._cam_y, w, h
        self.set_color_(color)
        SDL_RenderFillRect(self._ren, d)

    def line(self, color: ColorType, x1: int, y1: int, x2: int, y2: int):
        self.set_color_(color)
        SDL_RenderDrawLine(self._ren,
            x1 - self._cam_x, y1 - self._cam_y,
            x2 - self._cam_x, y2 - self._cam_y
        )

    def circle(self, color: ColorType, x: int, y: int, radius: int):
        if len(color) == 3: color = (*color, 255)
        x -= self._cam_x
        y -= self._cam_y
        vert = self._circ_verts[0]
        c = vert.color
        c.r, c.g, c.b, c.a = color
        vert.position.x = x
        vert.position.y = y
        for i in range(1, CIRC_NGON + 1):
            vert = self._circ_verts[i]
            c = vert.color
            c.r, c.g, c.b, c.a = color
            vert.position.x = int(x + self._circ_angles[i-1][0] * radius)
            vert.position.y = int(y + self._circ_angles[i-1][1] * radius)
        if SDL_RenderGeometry(self._ren, None,
            self._circ_verts, self._num_circ_verts,
            self._circ_indices, self._num_circ_indices
        ) < 0:
            self._age.check_sdl_error_("SDL_RenderGeometry")

    def texture(self, tex: Texture,
        x: int, y: int,
        w:int=-1, h:int=-1,
        src:Tuple[int, int, int, int]=None
    ):
        if src is None:
            s = None
            if w == -1: w = tex.w
            if h == -1: h = tex.h
        else:
            s = self._src_rect
            s.x, s.y, s.w, s.h = src
            if w == -1: w = s.w
            if h == -1: h = s.h

        d = self._dst_rect
        d.x, d.y, d.w, d.h = x - self._cam_x, y - self._cam_y, w, h

        SDL_RenderCopy(self._ren, tex.tex_, s, d)

    def mod_texture(self, tex: Texture,
        x: int, y: int,
        w:int=-1, h:int=-1,
        src:Tuple[int, int, int, int]=None,
        flip_lr:bool=False, flip_tb:bool=False,
        angle:float=0.0, center:Tuple[int, int]=None
    ):
        """ * angle - clockwise; in degrees
            * center - anchor point of rotation; respects src and w,h """

        if src is None:
            s = None
            if w == -1: w = tex.w
            if h == -1: h = tex.h
        else:
            s = self._src_rect
            s.x, s.y, s.w, s.h = src
            if w == -1: w = s.w
            if h == -1: h = s.h

        d = self._dst_rect
        d.x, d.y, d.w, d.h = x - self._cam_x, y - self._cam_y, w, h

        if center is None:
            c = None
        else:
            c = self._point
            c.x, c.y = center

        SDL_RenderCopyEx(self._ren, tex.tex_,
            s, d,
            angle,
            c,
            self._flips[flip_lr, flip_tb]
        )

    def pixel(self, color: ColorType, x: int, y: int):
        self.set_color_(color)
        SDL_RenderDrawPoint(self._ren, x - self._cam_x, y - self._cam_y)

    def blend_paint(self, blend: bool):
        if blend: self.set_blend_mode_(SDL_BLENDMODE_BLEND)
        else: self.set_blend_mode_(SDL_BLENDMODE_NONE)

    def target_texture(self, texture: Optional[Texture]) -> TargetTextureContext:
        return TargetTextureContext(self, texture)

    def set_target_texture(self, texture: Optional[Texture]):
        SDL_SetRenderTarget(self._ren, None if texture is None else texture.tex_)
        self._target_texture = texture

    def get_target_texture(self) -> Optional[Texture]:
        return self._target_texture

    def show(self):
        SDL_RenderPresent(self._ren)

    def set_canvas_res(self, w: int, h: int):
        SDL_RenderSetLogicalSize(self._ren, w, h)

    def set_pixel_perfect(self, b: bool):
        SDL_RenderSetIntegerScale(self._ren, b)

    def set_vsync(self, vsync: bool):
        if SDL_RenderSetVSync(self._ren, int(vsync)) != 0:
            self._age.check_sdl_error_("SDL_RenderSetVSync")

    def set_blend_mode_(self, blend_mode: int):
        SDL_SetRenderDrawBlendMode(self._ren, blend_mode)

    def get_sdl_renderer_(self) -> SDLRenderer:
        return self._ren

    def quit_(self):
        SDL_DestroyRenderer(self._ren)

    def set_color_(self, color: ColorType):
        if len(color) == 3:
            SDL_SetRenderDrawColor(self._ren, *color, 255)
        else:
            SDL_SetRenderDrawColor(self._ren, *color)

    @classmethod
    def close_(cls):
        for surface in tuple(cls._surfaces):
            surface.quit_()

        for texture in tuple(cls._textures):
            texture.quit_()

    @classmethod
    def register_texture_(cls, texture: Texture):
        cls._textures.append(texture)

    @classmethod
    def unregister_texture_(cls, texture: Texture):
        cls._textures.remove(texture)

    @classmethod
    def register_surface_(cls, surface: Surface):
        cls._surfaces.append(surface)

    @classmethod
    def unregister_surface_(cls, surface: Surface):
        cls._surfaces.remove(surface)

    def _init_sdl_renderer(self, win: SDLWindow):
        flags = SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE
        if self._s.vsync:
            flags |= SDL_RENDERER_PRESENTVSYNC
            self._s.vsync = False # disable VSync for later windows
        self._ren = SDL_CreateRenderer(win, -1, flags)
        if self._ren is None:
            self._age.check_sdl_error_("SDL_CreateRenderer", fatal=True)

        self.set_blend_mode_(SDL_BLENDMODE_BLEND)
