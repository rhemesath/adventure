from typing import ClassVar, Tuple, List, Dict
from ctypes import byref, c_int

from sdl2 import (
    SDL_Event, SDL_EventType,
    SDL_MOUSEMOTION, SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP,
    SDL_BUTTON, SDL_GetGlobalMouseState, Uint32,
    SDL_MouseMotionEvent, SDL_MouseButtonEvent,
    SDL_MouseWheelEvent, SDL_MOUSEWHEEL
)
import sdl2.mouse as sdl2_mouse


_buttons = (
    sdl2_mouse.SDL_BUTTON_LEFT,
    sdl2_mouse.SDL_BUTTON_MIDDLE,
    sdl2_mouse.SDL_BUTTON_RIGHT
)


class Mouse:
    event_types: ClassVar[Tuple[SDL_EventType]] = (
        SDL_MOUSEMOTION, SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP,
        SDL_MOUSEWHEEL
    )
    _global_button_state: ClassVar[Uint32] = 0
    _global_x: ClassVar[int] = 0
    _global_y: ClassVar[int] = 0

    @classmethod
    def get_button_state(cls, button: str) -> bool:
        return cls.get_button_state_(cls.button_str_as_sdl_button_(button))

    @classmethod
    def get_global_position(cls) -> Tuple[int, int]:
        return (cls._global_x, cls._global_y)

    def __init__(self):
        self.x = 0
        self.y = 0
        self._wheel_x = 0.0
        self._wheel_y = 0.0

        self._pressed: Dict[int, bool] = {}
        self._down_events: List[int] = []
        self._up_events: List[int] = []

    def is_pressed(self, button: str) -> bool:
        return self.is_pressed_(self.button_str_as_sdl_button_(button))

    def down_event(self, button: str) -> bool:
        return self.down_event_(self.button_str_as_sdl_button_(button))

    def up_event(self, button: str) -> bool:
        return self.up_event_(self.button_str_as_sdl_button_(button))

    def scroll_x(self) -> float:
        """ negative: to the left; positive: to the right """
        return self._wheel_x

    def scroll_y(self) -> float:
        """ negative: away from user (up); positive: to the user (down) """
        return self._wheel_y

    @staticmethod
    def button_str_as_sdl_button_(button: str) -> int:
        return getattr(sdl2_mouse, f"SDL_BUTTON_{button.upper()}")

    @staticmethod
    def get_event_window_id_(event: SDL_Event) -> Uint32:
        if event.type == SDL_MOUSEMOTION:
            return event.motion.windowID
        elif event.type in (SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP):
            return event.button.windowID
        elif event.type == SDL_MOUSEWHEEL:
            return event.wheel.windowID

    @classmethod
    def global_update_(cls):
        c_x, c_y = c_int(0), c_int(0)
        cls._global_button_state = SDL_GetGlobalMouseState(byref(c_x), byref(c_y))
        cls._global_x = c_x.value
        cls._global_y = c_y.value

    @classmethod
    def get_button_state_(cls, sdl_button: int) -> bool:
        return (SDL_BUTTON(sdl_button) & cls._global_button_state) != 0

    def is_pressed_(self, sdl_button: int) -> bool:
        return self._pressed.get(sdl_button, False)

    def down_event_(self, sdl_button: int) -> bool:
        return sdl_button in self._down_events

    def up_event_(self, sdl_button: int) -> bool:
        return sdl_button in self._up_events

    def flush_(self):
        self._down_events.clear()
        self._up_events.clear()
        self.rel_x = self.rel_y = 0
        self._wheel_x = self._wheel_y = 0.0

    def handle_sdl_event_(self, event: SDL_Event):
        if event.type == SDL_MOUSEMOTION:
            motion: SDL_MouseMotionEvent = event.motion
            self.x, self.y = motion.x, motion.y
            self.rel_x, self.rel_y = motion.xrel, motion.yrel

        elif event.type == SDL_MOUSEBUTTONDOWN:
            down: SDL_MouseButtonEvent = event.button
            self.x, self.y = down.x, down.y
            self._down_events.append(down.button)
            self._pressed[down.button] = True

        elif event.type == SDL_MOUSEBUTTONUP:
            up: SDL_MouseButtonEvent = event.button
            self.x, self.y = up.x, up.y
            self._up_events.append(up.button)
            self._pressed[up.button] = False

        elif event.type == SDL_MOUSEWHEEL:
            wheel: SDL_MouseWheelEvent = event.wheel
            self._wheel_x += wheel.preciseX
            self._wheel_y -= wheel.preciseY

    def update_(self):
        for sdl_button in _buttons:
            if not self.get_button_state_(sdl_button) and self.is_pressed_(sdl_button):
                self._pressed[sdl_button] = False
                self._up_events.append(sdl_button)
