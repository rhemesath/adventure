from typing import Dict, Optional
import sys

from sdl2 import (
    SDL_Init, SDL_Quit, SDL_INIT_VIDEO, SDL_GetError,
    SDL_Delay, SDL_Event, SDL_PollEvent, SDL_QUIT,
    SDL_GetTicks, SDL_WINDOWEVENT,
    SDL_INIT_AUDIO, SDL_INIT_GAMECONTROLLER,
    SDL_INIT_EVERYTHING, SDL_SetHint, SDL_HINT_APP_NAME,
    SDL_WINDOWEVENT_FOCUS_GAINED
)
from sdl2.sdlmixer import Mix_Init, Mix_Quit, MIX_INIT_MP3
from sdl2.sdlimage import IMG_Init, IMG_Quit, IMG_INIT_PNG

from .exceptions import AGEException
from .Settings import Settings
from .AGEObject import AGEObject
from .Window import Window
from .Painter import Painter
from .Audio import Audio
from .Keyboard import Keyboard
from .Mouse import Mouse
from .GamepadHandler import GamepadHandler


class AGE:
    def __init__(self, settings:Settings=None):
        self._s: Settings
        if settings is None: self._s = Settings()
        else: self._s = settings.copy()

        self.default_window: Optional[Window] = None
        self.win: Optional[Window] = None # current Window
        self.paint: Optional[Painter] = None # current Painter

        self.audio: Audio = None
        self.mouse: Mouse = Mouse()
        self.keyboard: Keyboard = Keyboard()
        self.gamepads: GamepadHandler = GamepadHandler()

        self._quit_requested = False
        self._event = SDL_Event()
        self._windows: Dict[int, Window] = {}
        self._focused_window: Optional[Window] = None

        AGEObject.setup_age_object_(self, self._s)
        self._init_sdl()

        Keyboard.setup_()
        self.audio = Audio()

        if (self._s.window_size, self._s.canvas_res) != (None, None):
            win = Window.from_settings()
            paint = win.create_painter_()
            if self._s.canvas_res is not None:
                paint.set_canvas_res(*self._s.canvas_res)
                win.set_min_size(*self._s.canvas_res)
            paint.set_pixel_perfect(self._s.pixel_perfect)

            paint.fill((0, 0, 0))
            paint.show()
            win.show()

            self.set_window(win)

    def update(self):
        self._quit_requested = False

        self.gamepads.flush_()
        self.keyboard.flush_()
        self.mouse.flush_()
        for window in self._windows.values():
            window.keyboard.flush_()
            window.mouse.flush_()

        e = self._event
        while SDL_PollEvent(e):
            type_ = e.type
            if type_ == SDL_QUIT:
                if len(self._windows) == 1:
                    tuple(self._windows.values())[0].handle_close_request_()
                elif len(self._windows) == 0:
                    self._quit_requested = True

            elif type_ in Keyboard.event_types:
                self.keyboard.handle_sdl_event_(e.key)
                win = self._windows.get(e.key.windowID)
                if win is not None: win.keyboard.handle_sdl_event_(e.key)

            elif type_ in Mouse.event_types:
                self.mouse.handle_sdl_event_(e)
                win = self._windows.get(Mouse.get_event_window_id_(e)) or self._focused_window
                if win is not None: win.mouse.handle_sdl_event_(e)

            elif type_ in self.gamepads.event_types:
                self.gamepads.handle_sdl_event_(e)

            elif type_ == SDL_WINDOWEVENT:
                win = self._windows.get(e.window.windowID)
                if win is not None:
                    if e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED:
                        self._focused_window = win
                    win.handle_sdl_event_(e.window)

        Mouse.global_update_()
        self.mouse.update_()
        for window in self._windows.values(): window.update_()
        self.gamepads.update_()

    def quit_requested(self) -> bool:
        return self._quit_requested

    def wait(self, seconds: float):
        SDL_Delay(int(1000 * seconds))

    def get_runtime(self) -> float:
        return SDL_GetTicks() / 1000

    def set_window(self, window: Optional[Window]):
        if self.default_window is None: self.default_window = window
        self.win = window
        self.paint = window.paint

    def quit(self):
        self.gamepads.quit_()
        if self.audio is not None: self.audio.quit_()
        Painter.close_()
        for window in tuple(self._windows.values()):
            window.quit_()
        Mix_Quit()
        IMG_Quit()
        SDL_Quit()

    def get_focused_window(self) -> Optional[Window]:
        """ returns the last focused window """
        return self._focused_window

    def check_sdl_error_(self, sdl_func_name: str, fatal:bool=False, ignore:bool=False):
        err = SDL_GetError().decode()
        if err == "": return
        err = f"{sdl_func_name}: {err}"
        if fatal:
            raise AGEException(err)
        elif not self._s.hide_warnings and not ignore:
            print(err, file=sys.stderr)

    def register_window_(self, window: Window):
        self._windows[window.get_id_()] = window

    def unregister_window_(self, window: Window):
        self._windows.pop(window.get_id_())
        if len(self._windows) == 0:
            if window is self.win: self.win = None
            if window.paint is self.paint: self.paint = None
        else:
            other_win = tuple(self._windows.values())[0]
            if window is self.win: self.win = other_win
            if window.paint is self.paint: self.paint = other_win.paint

        if self._s.main_window_quits and window is self.default_window:
            self._quit_requested = True
        elif len(self._windows) == 0 and window.closed_by_request_:
            self._quit_requested = True

    def _init_sdl(self):
        SDL_SetHint(SDL_HINT_APP_NAME, self._s.title.encode())

        flags = SDL_INIT_VIDEO | SDL_INIT_AUDIO
        if self._s.use_gamepads:
            flags |= SDL_INIT_GAMECONTROLLER
            flags = SDL_INIT_EVERYTHING # FIXME bugfix for harmless but annoying udev problems on Linux...
        if SDL_Init(flags) < 0:
            self.check_sdl_error_("SDL_Init", fatal=True)

        flags = IMG_INIT_PNG
        v = IMG_Init(flags)
        self.check_sdl_error_("IMG_Init", fatal=flags & v != flags)

        flags = MIX_INIT_MP3
        v = Mix_Init(flags)
        self.check_sdl_error_("Mix_Init", fatal=flags & v != flags)

        if self._s.use_gamepads:
            self.gamepads.init_sdl_()
