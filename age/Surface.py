from typing import Union, ClassVar, Tuple, Optional
from pathlib import Path
from ctypes import POINTER, byref

from sdl2 import (
    SDL_FreeSurface, SDL_CreateRGBSurface, SDL_SetPaletteColors,
    SDL_CreateRGBSurfaceWithFormat, SDL_PIXELFORMAT_RGBA32,
    SDL_FillRect, Uint32, SDL_MapRGBA, SDL_PixelFormat, SDL_Rect, SDL_BlitSurface
)
from sdl2.sdlimage import IMG_Load

from .types import SDLSurface, SurfaceColorType
from .exceptions import AGEException
from .AGEObject import AGEObject
from .Camera import Camera
from .Palette import Palette


class Surface(AGEObject, Camera):
    _src_rect: ClassVar[SDL_Rect] = SDL_Rect()
    _dst_rect: ClassVar[SDL_Rect] = SDL_Rect()

    sf_: SDLSurface
    w: int
    h: int
    has_palette: bool

    _format: POINTER(SDL_PixelFormat)

    def __init__(self, painter: "Painter", sf: SDLSurface, has_palette:bool=False):
        self.sf_ = sf
        self.has_palette = has_palette
        self.w = sf.contents.w
        self.h = sf.contents.h

        self._painter = painter
        self._format = sf.contents.format

        self._painter.register_surface_(self)

    @classmethod
    def create(cls, w: int, h: int, painter:Optional["Painter"]=None) -> "Surface":
        painter = painter or cls._age.paint
        if painter is None: raise AGEException("A Painter is required")

        sf: SDLSurface = SDL_CreateRGBSurfaceWithFormat(0, # unused flag
            w, h, 32,
            SDL_PIXELFORMAT_RGBA32
        )
        cls._age.check_sdl_error_("SDL_CreateRGBSurfaceWithFormat", fatal=sf is None)

        return cls(painter, sf)

    @classmethod
    def create_8bit(cls, w: int, h: int, palette: Palette, painter:Optional["Painter"]=None) -> "Surface":
        painter = painter or cls._age.paint
        if painter is None: raise AGEException("A Painter is required")

        sf: SDLSurface = SDL_CreateRGBSurface(0, # unused flag
            w, h, 8, # width, height, bits per pixel
            0, 0, 0, 0 # unused rgba color masks for true color
        )
        cls._age.check_sdl_error_("SDL_CreateRGBSurface", fatal=sf is None)

        cls._set_palette(sf, palette, 0)

        return cls(painter, sf, has_palette=True)

    @classmethod
    def load(cls, path: Union[Path, str], painter:Optional["Painter"]=None) -> "Surface":
        painter = painter or cls._age.paint
        if painter is None: raise AGEException("A Painter is required")

        if type(path) is str: path = Path(path)

        sf: SDLSurface = IMG_Load(str(path).encode())
        cls._age.check_sdl_error_("IMG_Load", fatal=sf is None)

        return cls(painter, sf)

    @classmethod
    def _set_palette(cls, sf: SDLSurface, palette: Palette, index: int):
        SDL_SetPaletteColors(
            sf.contents.format.contents.palette,
            palette.get_sdl_colors_(),
            index,
            palette.num_colors
        )
        #cls._age.check_sdl_error_("SDL_SetPaletteColors")

    def set_palette(self, palette: Palette):
        self._set_palette(self.sf_, palette, 0)

    def change_palette_color(self, index: int, color: Tuple[int, int, int]):
        self._set_palette(self.sf_, Palette(color), index)

    def draw_surface(self, other: "Surface", x: int, y: int,
        src:Tuple[int, int, int, int]=None
    ):
        d = self._dst_rect
        d.x, d.y = x - self._cam_x, y - self._cam_y

        if src is None:
            s = None
            d.w, d.h = other.w, other.h
        else:
            s = self._src_rect
            s.x, s.y, s.w, s.h = src
            d.w, d.h = s.w, s.h

        SDL_BlitSurface(other.sf_, byref(s), self.sf_, byref(d))

    def draw_rect(self, color: SurfaceColorType, x: int, y: int, w: int, h: int):
        d = self._dst_rect
        d.x, d.y, d.w, d.h = x - self._cam_x, y - self._cam_y, w, h
        SDL_FillRect(self.sf_, byref(d), self.get_color_(color))

    def fill(self, color: SurfaceColorType):
        SDL_FillRect(self.sf_, None, self.get_color_(color))

    def quit_(self):
        self._painter.unregister_surface_(self)
        SDL_FreeSurface(self.sf_)

    def get_color_(self, color: SurfaceColorType) -> Union[int, Uint32]:
        if type(color) is int:
            return color
        elif len(color) == 3:
            return SDL_MapRGBA(self._format, *color, 255)
        else:
            return SDL_MapRGBA(self._format, *color)


from . import Painter
