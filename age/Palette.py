from typing import Tuple
from ctypes import POINTER, cast

from sdl2 import SDL_Color

from .AGEObject import AGEObject


class Palette(AGEObject):
    def __init__(self, *colors: Tuple[Tuple[int, int, int]]):
        self._colors = colors
        self.num_colors = len(colors)

        self._sdl_colors: POINTER(SDL_Color) = cast(
            (SDL_Color * self.num_colors)(*[color for color in self._colors]),
            POINTER(SDL_Color)
        )

    def get_sdl_colors_(self) -> POINTER(SDL_Color):
        return self._sdl_colors
