from typing import List

from sdl2 import AUDIO_S16
from sdl2.sdlmixer import (
    Mix_OpenAudio,
    Mix_CloseAudio, Mix_PlayChannel, Mix_Volume, MIX_MAX_VOLUME,
    Mix_PlayMusic, Mix_VolumeMusic, Mix_PauseMusic, Mix_ResumeMusic,
    Mix_RewindMusic, Mix_SetMusicPosition, Mix_RegisterEffect, Mix_UnregisterEffect,
    MIX_CHANNEL_POST, Mix_UnregisterAllEffects
)

from .AGEObject import AGEObject
from .Sound import Sound
from .Music import Music
from .audio_effects import AudioEffect


class Audio(AGEObject):
    def __init__(self):
        self._sounds: List[Sound] = []
        self._musics: List[Music] = []

        self._sounds_volume: float = 1.0
        self._music_volume: float = 1.0

        self._init_sdl_mixer()

    def play_sound(self, sound: Sound, loops:int=0):
        """ -1 for infinite loops """
        if Mix_PlayChannel(-1, sound.data_, loops) < 0:
            self._age.check_sdl_error_("Mix_PlayChannel")

    def play_music(self, music: Music, loops:int=0):
        """ -1 for infinite loops """
        if Mix_PlayMusic(music.data_, loops) < 0:
            self._age.check_sdl_error_("Mix_PlayMusic")

    def set_sounds_volume(self, volume: float):
        """ 0.0 <= volume <= 1.0 """
        self._sounds_volume = volume
        Mix_Volume(-1, min(max(int(volume * MIX_MAX_VOLUME), 0), MIX_MAX_VOLUME))

    def set_music_volume(self, volume: float):
        """ 0.0 <= volume <= 1.0 """
        self._music_volume = volume
        Mix_VolumeMusic(min(max(int(volume * MIX_MAX_VOLUME), 0), MIX_MAX_VOLUME))

    def get_sounds_volume(self) -> float:
        return self._sounds_volume

    def get_music_volume(self) -> float:
        return self._music_volume

    def pause_music(self):
        Mix_PauseMusic()

    def resume_music(self):
        Mix_ResumeMusic()

    def rewind_music(self):
        Mix_RewindMusic()

    def skip_music_seconds(self, seconds: float):
        Mix_SetMusicPosition(seconds)

    def set_music_pos(self, seconds: float):
        self.rewind_music()
        self.skip_music_seconds(seconds)

    def add_audio_effect(self, effect: AudioEffect):
        if Mix_RegisterEffect(
            MIX_CHANNEL_POST, effect.effect_, effect.cleanup_, None
        ) == 0:
            self._age.check_sdl_error_("Mix_RegisterEffect")

    def remove_audio_effect(self, effect: AudioEffect):
        if Mix_UnregisterEffect(
            MIX_CHANNEL_POST, effect.effect_
        ) == 0:
            self._age.check_sdl_error_("Mix_UnregisterEffect")

    def remove_all_effects(self):
        Mix_UnregisterAllEffects(MIX_CHANNEL_POST)
        self.set_sounds_volume(self._sounds_volume)
        self.set_music_volume(self._music_volume)

    def quit_(self):
        for sound in tuple(self._sounds):
            sound.quit_()

        for music in tuple(self._musics):
            music.quit_()

        Mix_CloseAudio()

    def register_sound_(self, sound: Sound):
        self._sounds.append(sound)

    def register_music_(self, music: Music):
        self._musics.append(music)

    def unregister_sound_(self, sound: Sound):
        self._sounds.remove(sound)

    def unregister_music_(self, music: Music):
        self._musics.remove(music)

    def _init_sdl_mixer(self):
        if Mix_OpenAudio(
            self._s.audio_frequency, AUDIO_S16,
            self._s.audio_channels,
            2 ** max(0, 9 + self._s.audio_buffer_size) # default chunksize of 512
        ) < 0:
            self._age.check_sdl_error_("Mix_OpenAudio", fatal=True)
