from typing import Optional, Dict
from pathlib import Path

from sdl2 import (
    SDL_GameControllerAddMappingsFromFile,
    SDL_NumJoysticks, SDL_GameControllerNameForIndex,
    SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLERBUTTONUP,
    SDL_CONTROLLERAXISMOTION, SDL_Event,
    SDL_ControllerButtonEvent, SDL_ControllerAxisEvent,
    SDL_PRESSED
)

from .AGEObject import AGEObject
from .Gamepad import Gamepad


class GamepadHandler(AGEObject):
    def __init__(self):
        self.event_types = (
            SDL_CONTROLLERBUTTONDOWN, SDL_CONTROLLERBUTTONUP, SDL_CONTROLLERAXISMOTION
        )
        self._gpads: Dict[int, Gamepad] = {}

    def get_num_available(self) -> int:
        num = SDL_NumJoysticks()
        if num < 0:
            self._age.check_sdl_error_("SDL_NumJoysticks")
        return max(0, num)

    def get_name(self, index: int) -> str:
        name: Optional[bytes] = SDL_GameControllerNameForIndex(index)
        if not name:
            self._age.check_sdl_error_("SDL_GameControllerNameForIndex")
            return "Unknown"
        return name.decode()

    def handle_sdl_event_(self, event: SDL_Event):
        if event.type == SDL_CONTROLLERBUTTONDOWN or event.type == SDL_CONTROLLERBUTTONUP:
            self._handle_button_event(event.cbutton)
        elif event.type == SDL_CONTROLLERAXISMOTION:
            self._handle_axis_event(event.caxis)

    def register_(self, gamepad: Gamepad):
        self._gpads[gamepad.id] = gamepad

    def unregister_(self, gamepad: Gamepad):
        self._gpads.pop(gamepad.id)

    def _handle_button_event(self, event: SDL_ControllerButtonEvent):
        gamepad = self._gpads.get(event.which)
        if gamepad is None: return
        gamepad.handle_button_event_(event.button, event.state == SDL_PRESSED)

    def _handle_axis_event(self, event: SDL_ControllerAxisEvent):
        gamepad = self._gpads.get(event.which)
        if gamepad is None: return
        gamepad.handle_axis_motion_event_(event.axis, event.value)

    def flush_(self):
        for gamepad in self._gpads.values():
            gamepad.flush_()

    def update_(self):
        for gamepad in self._gpads.values():
            gamepad.update_()

    def init_sdl_(self):
        db_path = Path(__file__).parent.joinpath("gamecontrollerdb.txt")

        if SDL_GameControllerAddMappingsFromFile(str(db_path).encode()) < 0:
            self._age.check_sdl_error_("SDL_GameControllerAddMappingFromFile", fatal=True)

    def quit_(self):
        for gamepad in tuple(self._gpads.values()):
            gamepad.quit_()
