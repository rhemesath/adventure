from typing import Union, Callable, Tuple
from ctypes import POINTER

from sdl2 import (
    SDL_Window, SDL_Renderer, SDL_Texture, SDL_Surface,
    SDL_GameController
)
from sdl2.sdlmixer import Mix_Chunk, Mix_Music


SDLWindow   = POINTER(SDL_Window)
SDLRenderer = POINTER(SDL_Renderer)
SDLTexture  = POINTER(SDL_Texture)
SDLSurface  = POINTER(SDL_Surface)
MixChunk    = POINTER(Mix_Chunk)
MixMusic   = POINTER(Mix_Music)
SDLGameController = POINTER(SDL_GameController)


ColorType = Union[Tuple[int, int, int, int], Tuple[int, int, int]]
SurfaceColorType = Union[int, ColorType]

SoundWave = Callable[[float], float] # s(t) -> [-1.0, 1.0]; t: [0.0 -> length], in seconds
