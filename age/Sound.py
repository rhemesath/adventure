from typing import Union
from pathlib import Path

from ctypes import POINTER, cast, c_ubyte, c_uint32, c_int16

from sdl2.sdlmixer import (
    Mix_QuickLoad_RAW,
    Mix_LoadWAV, Mix_FreeChunk
)

from .AGEObject import AGEObject
from .types import MixChunk, SoundWave


class Sound(AGEObject):
    data_: MixChunk = None

    def __init__(self, data: MixChunk):
        self.data_ = data

        self._age.audio.register_sound_(self)

    @classmethod
    def create(cls, wave: SoundWave, length_secs: float) -> "Sound":
        # sample range: signed 16 bit -> [-32768, 32767]
        # TODO test
        frequency = cls._age._s.audio_frequency
        num_channels = cls._s.audio_channels

        num_samples = int(frequency * length_secs)
        samples = (c_int16 * num_samples * num_channels)()

        step_size: float = 1 / frequency
        for sample_i, value in enumerate([
            min(max(
                int(wave(step * step_size) * 32768),
            -32768), 32767)
            for step in range(num_samples)
        ]):
            for channel_i in range(num_channels):
                samples[sample_i * num_channels + channel_i] = value

        data: MixChunk = Mix_QuickLoad_RAW(
            cast(samples, POINTER(c_ubyte)),
            len(samples) * 2
        )
        if data is None:
            cls._age.check_sdl_error_("Mix_QuickLoad_RAW", fatal=True)

        return cls(data)

    @classmethod
    def load(cls, path: Union[Path, str]) -> "Sound":
        """ must be .wav file """
        if type(path) is str: path = Path(path)

        data: MixChunk = Mix_LoadWAV(str(path).encode())
        if data is None:
            cls._age.check_sdl_error_("Mix_LoadWAV", fatal=True)

        return cls(data)

    def quit_(self):
        self._age.audio.unregister_sound_(self)
        Mix_FreeChunk(self.data_)
