from typing import Optional, Tuple
from ctypes import byref, c_int

from sdl2 import (
    SDL_CreateWindow, SDL_DestroyWindow, SDL_WINDOWPOS_UNDEFINED,
    SDL_ShowWindow, SDL_WINDOW_HIDDEN, SDL_WINDOW_RESIZABLE,
    SDL_SetWindowMinimumSize, SDL_WINDOWEVENT_SIZE_CHANGED, SDL_WindowEvent,
    SDL_GetWindowID, SDL_WINDOWEVENT_CLOSE, SDL_HideWindow, SDL_GetWindowSize,
    SDL_WINDOWEVENT_FOCUS_GAINED, SDL_WINDOWEVENT_FOCUS_LOST
)

from .types import SDLWindow
from .exceptions import AGEException
from .AGEObject import AGEObject
from .Painter import Painter
from .Mouse import Mouse
from .Keyboard import Keyboard


class Window(AGEObject):
    def __init__(self, title: str, w: int, h: int, resizable:bool=False):
        self._win: SDLWindow = None
        self.paint: Optional[Painter] = None
        self.mouse = Mouse()
        self.keyboard = Keyboard()
        self.closed_by_request_ = False

        self._clear_input = False

        self._init_sdl_window(title, w, h, resizable)
        self._age.register_window_(self)

    @classmethod
    def from_settings(cls) -> "Window":
        win_size = cls._s.window_size
        if win_size is None:
            if cls._s.canvas_res is None:
                raise AGEException("No window size or resolution defined in Settings")
            win_size = cls._s.canvas_res

        return cls(cls._s.title, *win_size, cls._s.window_resizable)

    def set_min_size(self, w: int, h: int):
        SDL_SetWindowMinimumSize(self._win, w, h)

    def get_size(self) -> Tuple[int, int]:
        c_w, c_h = c_int(0), c_int(0)
        SDL_GetWindowSize(self._win, byref(c_w), byref(c_h))
        return (c_w.value, c_h.value)

    def show(self):
        SDL_ShowWindow(self._win)

    def hide(self):
        SDL_HideWindow(self._win)

    def create_painter_(self) -> Optional[Painter]:
        if self._win is None or self.paint is not None: return None
        self.paint = Painter(self._win)
        return self.paint

    def update_(self):
        self.mouse.update_()

        if self._clear_input:
            self._clear_input = False
            self.keyboard.flush_()
            self.mouse.flush_()

    def handle_sdl_event_(self, event: SDL_WindowEvent):
        event_type = event.event
        if event_type == SDL_WINDOWEVENT_SIZE_CHANGED:
            self.on_resize_()
        elif event_type == SDL_WINDOWEVENT_CLOSE:
            self.handle_close_request_()
        elif event_type == SDL_WINDOWEVENT_FOCUS_GAINED:
            self._clear_input = True
            self.on_focus_()
        elif event_type == SDL_WINDOWEVENT_FOCUS_LOST:
            self.on_unfocus_()

    def quit_(self):
        if self.paint is not None: self.paint.quit_()
        self._age.unregister_window_(self)
        SDL_DestroyWindow(self._win)

    def get_id_(self) -> int:
        return SDL_GetWindowID(self._win)

    def handle_close_request_(self):
        if self.do_close_():
            self.closed_by_request_ = True
            self.quit_()

    def on_focus_(self):
        """ Override if needed """
        pass

    def on_unfocus_(self):
        """ Override if needed """
        pass

    def on_resize_(self):
        """ Override if needed """
        if self.paint is not None: self.paint.fill((0, 0, 0))

    def do_close_(self) -> bool:
        """ Override if needed. Is called when the user wants to close this Window """
        return True

    def _init_sdl_window(self, title: str, w: int, h: int, resizable: bool):
        flags = SDL_WINDOW_HIDDEN
        if resizable: flags |= SDL_WINDOW_RESIZABLE

        self._win = SDL_CreateWindow(title.encode(),
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            w, h, flags
        )
        if self._win is None:
            self._age.check_sdl_error_("SDL_CreateWindow", fatal=True)
