from typing import Optional
from ctypes import byref, c_int, c_void_p, c_size_t, POINTER

from sdl2 import (
    SDL_LockTexture, SDL_UnlockTexture, SDL_BlitSurface, SDL_memcpy
)

from .types import SDLSurface
from .exceptions import AGEException
from .AGEObject import AGEObject
from .Texture import Texture
from .Surface import Surface
from .Painter import Painter


class SurfaceStream(AGEObject):
    texture: Texture # the stream destination texture

    _src_sf:  SDLSurface
    _conv_sf: Surface
    _dest_tex_borrowed: bool

    _dest_ptr: POINTER(c_void_p)
    _pitch: c_int
    _num_bytes: c_size_t

    def __init__(self, src_surface: Surface, dest_texture:Optional[Texture]=None, painter:Optional[Painter]=None):
        """ dest_texture must be a streaming Texture """
        w, h = src_surface.w, src_surface.h

        if dest_texture is None:
            self.texture = Texture.create(w, h, streaming=True, painter=(painter or src_surface.get_painter_()))
            self._dest_tex_borrowed = False
        elif not dest_texture.streaming:
            raise AGEException(f"`dest_texture ({dest_texture})` is not a streaming texture")
        else:
            self.texture = dest_texture
            self._dest_tex_borrowed = True

        self._src_sf  = src_surface.sf_
        self._conv_sf = Surface.create(w, h)

        self._dest_ptr = c_void_p()
        self._pitch = c_int()
        # texture and surface are 32 bit -> 4 bytes per pixel
        self._num_bytes = c_size_t(w * h * 4)

    def stream(self):
        converted: SDLSurface = self._conv_sf.sf_
        # copy source surface to a true color surface to
        # simply convert from palette to true color
        SDL_BlitSurface(self._src_sf, None, converted, None)
        # prepare stream
        SDL_LockTexture(self.texture.tex_, None,
            byref(self._dest_ptr), byref(self._pitch)
        )
        # stream true color surface pixels to the locked texture
        SDL_memcpy(self._dest_ptr, converted.contents.pixels, self._num_bytes)
        # finish
        SDL_UnlockTexture(self.texture.tex_)

    def quit(self):
        self.quit_()

    def quit_(self):
        self._conv_sf.quit_()
        if not self._dest_tex_borrowed:
            self.texture.quit_()
