from typing import Union
from pathlib import Path

from sdl2.sdlmixer import (
    Mix_LoadMUS, Mix_FreeMusic
)

from .AGEObject import AGEObject
from .types import MixMusic


class Music(AGEObject):
    data_: MixMusic

    def __init__(self, data: MixMusic):
        self.data_ = data

        self._age.audio.register_music_(self)

    @classmethod
    def load(cls, path: Union[Path, str]) -> "Music":
        """ must be .mp3 file """
        if type(path) is str: path = Path(path)

        data: MixMusic = Mix_LoadMUS(str(path).encode())
        if data is None:
            cls._age.check_sdl_error_("Mix_LoadMUS", fatal=True)

        return cls(data)

    def quit_(self):
        self._age.audio.unregister_music_(self)
        Mix_FreeMusic(self.data_)
