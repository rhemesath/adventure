import shared
import tiles
from item import Item
from unlock_particle import UnlockParticle
from twinkle_particle import TwinkleParticle


def _rumble():
    shared.controller.rumble(0.03, 0.015)


def _use_ladder(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    if shared.game.world.get_real_tile(x, y) is tiles.HOLE_UP:
        _rumble()
        shared.sound.play("use_item")
        shared.game.world.set_tile_(tiles.LADDER_UP, x, y)
        return True
    return False


def _use_rock(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    world = shared.game.world
    floor = world.get_floor_tile(x, y)
    tile  = world.get_tile(x, y)
    if not floor.type_.solid:
        if tile is None:
            world.set_tile(tiles.ROCKS, x, y)
        elif tile is tiles.CAMPFIRE_OFF:
            from bomb_entity import BombEntity
            world.set_tile(tiles.CAMPFIRE_ON, x, y)
            for ty in range(y - 1, y + 2):
                for tx in range(x - 1, x + 2):
                    if tx == 0 and ty == 0: continue
                    t = world.get_tile(tx, ty)
                    if t is not None and t.type_ == tiles.STONE.type_:
                        b = BombEntity(tx * 16 + 8, ty * 16 + 8, 0)
                        shared.game.ent_man.add(b)
                        b.explode()
                        shared.game.cam.shake(24, 0.85)
        else:
            return False
        _rumble()
        shared.sound.play("use_item")
        return True
    return False


def _use_pickaxe(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    tile = shared.game.world.get_tile(x, y)
    if   tile is tiles.ROCKS: tile = tiles.CLEAN_ROCKS
    elif tile is tiles.STONE: tile = tiles.BRICK_WALL
    else: return False
    _rumble()
    shared.sound.play("use_item")
    shared.game.world.set_tile(tile, x, y)


def _use_bricks(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    world = shared.game.world
    floor = world.get_floor_tile(x, y)
    if world.get_tile(x, y) is None and not floor.type_.solid:
        _rumble()
        shared.sound.play("use_item")
        world.set_tile(tiles.BRICK_WALL, x, y)
        return True
    return False


def _use_bomb(x: int, y: int) -> bool:
    shared.sound.play("bomb_throw")
    shared.game.ent_man.add(BombEntity(x, y, shared.game.player.cross_dir))
    return True


def _use_key(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    world = shared.game.world
    tile = world.get_tile(x, y)
    if type(tile) is tiles.LockedTile:
        world.set_tile(tile.type_.get(), x, y)

        _rumble()
        shared.sound.play("unlock")
        shared.game.ent_man.add(UnlockParticle(x, y))
        cx, cy = x * 16 + 8, y * 16 + 8
        for _ in range(8):
            shared.game.ent_man.add(TwinkleParticle(cx, cy))

        return True
    return False


def _use_destroyer(x: int, y: int) -> bool:
    shared.game.player.take_damage(None, 1)
    return False


def _use_stick(x: int, y: int) -> bool:
    x, y = x // 16, y // 16
    world = shared.game.world
    floor = world.get_floor_tile(x, y)
    if world.get_tile(x, y) is None and not floor.type_.solid:
        _rumble()
        shared.sound.play("use_item")
        world.set_tile(tiles.CAMPFIRE_OFF, x, y)
        return True
    return False


def _get_lantern():
    shared.game.player.light_radius = 90


HAND   = Item("hand", 0,11, damage=1)
STICK  = Item("stick", 0.5,11, damage=2, stackable=True, use=_use_stick)
ROCK   = Item("rock",  0,11.5, damage=3, stackable=True, use=_use_rock)
LADDER = Item("ladder", 0.5,11.5, use=_use_ladder)
DESTROYER = Item("destroyer", 1,10, damage=999, use=_use_destroyer)
BRICKS    = Item("bricks", 1,11.5, stackable=True, use=_use_bricks)
PICKAXE   = Item("pickaxe", 1,11, damage=6, use=_use_pickaxe)
BOMB      = Item("bomb", 1.5,11, stackable=1, use=_use_bomb)
KEY       = Item("key", 1.5,11.5, stackable=1, use=_use_key)
LANTERN   = Item("lantern", 2,11, get=_get_lantern)


from bomb_entity import BombEntity
