from age import AGE, Painter, Texture, DelayAudioEffect

from sound import Sound
from controller import Controller
from tile_animator import TileAnimator


game: "Game"
age: AGE
paint: Painter
main_tex: Texture
sound: Sound
controller: Controller
cave_audio_effect: DelayAudioEffect

tile_animator = TileAnimator()


def setup(game_: "Game"):
    global age, game, paint, main_tex, controller, sound, cave_audio_effect
    game = game_
    age = game.age
    paint = age.paint
    main_tex = game.main_tex
    sound = game.sound
    controller = game.controller
    cave_audio_effect = DelayAudioEffect(0.15)


from game import Game
