from typing import Final, Dict, List

from tile import TileType, Tile, ContainerTileType, ContainerTile, LockedTile
from constants import FLOOR_LAYER
from images import (
    img_particle_stone, img_particle_leaves, img_particle_wood, img_particle_misc,
    ani_tile_water, ani_campfire
)

predefined_tiles: Final[Dict[int, List[Tile]]] = {}


T_FLOOR   = TileType("floor", layer=FLOOR_LAYER)
T_TILE    = TileType("tile")
T_WATER   = TileType("water", layer=FLOOR_LAYER, solid=True)
T_FLOWERS = TileType("flowers", health=1, particle=img_particle_misc)
T_WEED    = TileType("weed", health=1, particle=img_particle_leaves)
T_CAVE_STONE = TileType("cave stone", solid=True, health=18, min_damage=6, particle=img_particle_stone)

GRASS   = Tile.new(1, T_FLOOR)
ROCKS   = Tile.new(6, TileType("rocks", health=1, particle=img_particle_stone))
STONE   = Tile.new(5, TileType("stone", solid=True, health=9, min_damage=3, particle=img_particle_stone))
TREE    = Tile.new(4, TileType("tree",  solid=True, health=3, particle=img_particle_leaves))
TREE_LEAFLESS = Tile.new(9,  TileType("leafless tree", solid=True, health=6, particle=img_particle_wood))
TREE_STUMP    = Tile.new(10, TileType("tree stump", solid=True, health=12, particle=img_particle_wood))
WATER   = Tile.new(ani_tile_water, T_WATER)
CAVE_FLOOR  = Tile.new(13, T_FLOOR)
LADDER_DOWN = Tile.new(15, T_TILE)
HOLE_DOWN   = Tile.new(16, T_FLOOR)
LADDER_UP   = Tile.new(17, T_TILE)
HOLE_UP     = Tile.new(18, T_FLOOR)
CHEST_OPEN  = Tile.new(20, TileType("open chest", solid=True, health=6, particle=img_particle_wood))
CLEAN_ROCKS = Tile.new(21, TileType("clean rocks", health=1, particle=img_particle_stone))
BRICK_WALL  = Tile.new(22, TileType("bricks", solid=True, health=30, min_damage=6, particle=img_particle_stone))
CAMPFIRE_OFF = Tile.new(25, TileType("campfire off", solid=False, health=1, particle=img_particle_wood))
CAMPFIRE_ON  = Tile.new(ani_campfire, TileType("campfire on", solid=False, health=1, particle=img_particle_wood))


# add replace tiles
for tile_type, replace_tile in {
    TREE.type_: {12: None, 1: TREE_LEAFLESS},
    TREE_LEAFLESS.type_: TREE_STUMP,
    STONE.type_: ROCKS
}.items():
    tile_type.set(replace_tile)


# add items
from items import STICK, ROCK, BRICKS, KEY, LADDER, PICKAXE, LANTERN

for tile_type, item in {
    TREE_LEAFLESS.type_: STICK,
    ROCKS.type_: ROCK,
    CLEAN_ROCKS.type_: BRICKS,
    BRICK_WALL.type_: BRICKS,
}.items():
    tile_type.set_item(item)


CHEST_KEY     = ContainerTile.new(19, ContainerTileType("", item=KEY, solid=True, tile=CHEST_OPEN))
CHEST_LADDER  = ContainerTile.new(19, ContainerTileType("", item=LADDER, solid=True, tile=CHEST_OPEN))
CHEST_PICKAXE = ContainerTile.new(19, ContainerTileType("", item=PICKAXE, solid=True, tile=CHEST_OPEN))
CHEST_LANTERN = ContainerTile.new(19, ContainerTileType("", item=LANTERN, solid=True, tile=CHEST_OPEN))

LOCKED_LADDER_CHEST = LockedTile.new(23, TileType("locked ladder chest", solid=True, tile=CHEST_LADDER))


predefined_tiles.update(Tile.aliases.copy())
