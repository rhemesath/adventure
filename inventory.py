from typing import List, Dict

import shared
from constants import BTN_L, BTN_R
from item import Item
from items import HAND


class Inventory:
    def __init__(self):
        self._items: List[Item] = []
        self._item_count: Dict[Item, int] = {}
        self._index = 0

        self.add(HAND)

    def add(self, item: Item, amount:int=1):
        if item in self._items:
            if item.stackable: self._item_count[item] += amount
        else:
            self._items.append(item)
            self._item_count[item] = amount
        if item.get is not None: item.get()

    def remove(self, item: Item):
        self._item_count[item] -= 1
        if self._item_count[item] == 0:
            if self.item is item:
                self._index -= 1
            self._items.remove(item)

    @property
    def item(self) -> Item:
        return self._items[self._index]

    def update(self):
        if shared.controller.down(BTN_L) and self._index > 0:
            self._index -= 1
        elif shared.controller.down(BTN_R) and self._index < len(self._items) - 1:
            self._index += 1

    def render(self):
        c_item = self.item
        text = c_item.name
        if c_item.stackable: text += f"*{self._item_count[c_item]}"

        tex = shared.main_tex

        shared.game.text.render(text, 0, 8)
        for i, item in enumerate(self._items):
            x = i * 10
            shared.paint.rect((50, 85, 100) if item is not c_item else (100, 150, 190), x, 0, 10, 8)
            shared.paint.texture(tex, x+1, 0, src=item.gfx.get_rect())
