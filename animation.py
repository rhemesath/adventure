from typing import Tuple

from frame import Frame


class Animation:
    def __init__(self, frames: Tuple[Frame], frame_duration: int, loop:int=0):
        self.frames = frames
        self.frame_duration = frame_duration
        self.loop = bool(loop)

        self._num_frames = len(frames)
        self.length = self._num_frames * frame_duration

    def get_frame(self, counter: int) -> Frame:
        index = counter // self.frame_duration
        if self.loop: index %= self._num_frames
        else: index = min(self._num_frames - 1, index)
        return self.frames[index]

    def is_done(self, counter: int):
        if self.loop: return False
        return counter // self.frame_duration > self._num_frames


class TileAnimation:
    def __init__(self, frames: Tuple[int], frame_duration: int):
        self._frames = frames
        self.frame_duration = frame_duration
        self._num_frames = len(frames)

    def get_frame(self, counter: int) -> int:
        return self._frames[
            (counter // self.frame_duration) % self._num_frames
        ]
