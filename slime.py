from typing import Optional
from random import randint

import shared
from entity import Entity, State, KnockbackState
from frame import Frame as F
from animation import Animation
from images import img_shadow
from damage_particle import DamageParticle, DamageSwipe
from despawn_smoke import DespawnSmoke
from twinkle_particle import TwinkleParticle
from constants import STATE_IDLE, STATE_MOVING, DIRS, HIT


img_idle = F(0, 12)


class SlimeState(State):
    _user: "Slime"
    def on_tile_col(self): pass


class IdleState(SlimeState):
    name = STATE_IDLE

    def __init__(self, user: "Slime"):
        super().__init__(user)
        self._counter = 0
        self._wait = 2 * 60

    def enter(self):
        self._counter = self._wait

    def update(self):
        if self._counter == 0:
            self._user.change_state(self._user.move_state)
            return

        self._counter -= 1

    def get_frame(self) -> F:
        return img_idle


class MoveState(SlimeState):
    name = STATE_MOVING

    def __init__(self, user: "Slime"):
        super().__init__(user)
        self._dir_x = 0
        self._dir_y = 0
        self._counter = 0
        self._wait = 2 * 60

        self._ani = Animation((F(1, 12), F(0, 12)), 8, loop=1)

    def enter(self):
        # check which tiles are free
        dirs = []
        tile_x, tile_y = self._user.tile_x, self._user.tile_y
        for dx, dy in DIRS:
            x, y = tile_x + dx, tile_y + dy
            if not shared.game.world.get_real_tile(x, y).type_.solid:
                dirs.append((dx, dy))
        # try to not move back and forth
        back_dir = (-self._dir_x, -self._dir_y)
        if len(dirs) > 1 and back_dir in dirs: dirs.remove(back_dir)
        # no tiles are free, direction doesn't matter
        if len(dirs) == 0: dirs = DIRS

        self._dir_x, self._dir_y = dirs[randint(0, len(dirs) - 1)]

        self._counter = self._wait

    def update(self):
        if self._counter == 0:
            self._user.change_state(self._user.idle_state)
            return

        self._counter -= 1

    def get_frame(self) -> F:
        return self._ani.get_frame(self._counter)

    def move_x(self):
        self._user.xf += self._dir_x * self._user.vel

    def move_y(self):
        self._user.yf += self._dir_y * self._user.vel

    def on_tile_col(self):
        self._user.change_state(self._user.idle_state)


class SlimeKnockbackState(SlimeState, KnockbackState):
    def get_frame(self) -> F:
        return img_idle


class Slime(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(0, 0, 12, 12, shadow=img_shadow)
        self.center = (cx, cy)

        self._state: SlimeState
        self.idle_state = IdleState(self)
        self.move_state = MoveState(self)
        self.knockback_state = SlimeKnockbackState(self)

        self.vel = 0.5

        self.img: F

        self._img_align = self.align_cx_b

        self.check_collisions = True
        self.has_health = True
        self.health = 8
        self.damage = 1
        self._invulnerability_frames = 6

        self.change_state(self.idle_state)

    def set_frame(self):
        self._frame = self._state.get_frame()

    def update(self): self._state.update()

    def die(self):
        cx, cy = self.center
        shared.game.ent_man.add(DespawnSmoke(cx, cy))
        for _ in range(4):
            shared.game.ent_man.add(TwinkleParticle(cx, cy, vel_fac=0.1))
        shared.sound.play("entity_die")
        super().die()

    def move(self):
        self._state.move()
        super().move()

    def _move_x(self): self._state.move_x()
    def _move_y(self): self._state.move_y()

    def _on_tile_col_x(self, tile_x: int, tile_y: int, dx: float):
        self._state.on_tile_col()
    def _on_tile_col_y(self, tile_x: int, tile_y: int, dy: float):
        self._state.on_tile_col()

    def on_collision(self, entity: "Entity"):
        if entity is shared.game.player:
            entity.take_damage(self, self.damage)

    def check_health(self):
        super().check_health()
        if self.health <= 0: self.die()

    def take_damage(self, from_entity: Optional["Entity"], damage: int) -> int:
        prev_health = self.health
        v = super().take_damage(from_entity, damage)
        if v == HIT:
            shared.game.ent_man.add(DamageParticle(prev_health - self.health, self.center_x, self.y))
            shared.game.ent_man.add(DamageSwipe(self, left=bool(randint(0, 1))))
            if from_entity is not None:
                self.change_state(self.knockback_state)
                self.knockback_state.set_knockback(from_entity)
        return v
