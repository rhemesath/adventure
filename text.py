from typing import Optional, Dict, Tuple

from age import Texture, Painter

from constants import FONT_FILE


TextColorType = Optional[Tuple[int, int, int]]


class Text:
    def __init__(self, texture:Optional[Texture]=None):
        chars = ' 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.,!?:;"+-*/%&[]()'
        self._coords: Dict[str, int] = {
            char: i * 8
            for i, char in enumerate(chars)
        }

        self.font = texture

    def load_font(self, paint:Optional[Painter]=None):
        self.font = Texture.load(FONT_FILE, painter=paint)

    def render(self, text: str, x: int, y: int, size:int=8, color:TextColorType=None):
        paint_texture = self.font.get_painter_().texture

        if color is not None:
            self.font.set_color_mod(color)

        for char in text.upper():
            paint_texture(self.font, x, y, w=size, h=size, src=(self._coords[char], 0, 8, 8))
            x += size

        if color is not None:
            self.font.set_color_mod((255, 255, 255))

    def size(self, text: str, size:int=8) -> Tuple[int, int]:
        return (len(text) * size, size)
