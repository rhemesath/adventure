from random import uniform as rand

import shared
from entity import Entity
from item import Item
from images import img_shadow_small


class ItemEntity(Entity):
    def __init__(self, item: Item, cx: int, cy: int):
        super().__init__(0, 0, w=8, h=8, shadow=img_shadow_small)
        self.center = (cx, cy)

        self._vel_x = rand(-1, 1)
        self._vel_y = rand(-1, 1)
        self._vel_z = 2
        self._gravity = -0.2
        self._acc = 0.9
        self._bounciness = 0.8

        self._item = item
        self._collected = False

        self._frame = item.gfx

        self._bound_to_world = True

    def move(self):
        if self._collected:
            self.align_cx_b(self, shared.game.player)
            self.z += self._vel_z
            self._vel_z *= self._acc
        else:
            # check if player is in range
            player = shared.game.player
            tx, ty = self.center_x // 16, self.center_y // 16
            if tx-1 <= player.tile_cx <= tx+1 and ty-1 <= player.tile_cy <= ty+1:
                dx = player.center_x - self.center_x
                dy = player.center_y - self.center_y
                dis = (dx*dx + dy*dy)**0.5
                if dis <= 20:
                    fac = 0.1 / dis
                    self._vel_x = min(1, max(-1, self._vel_x + dx * fac))
                    self._vel_y = min(1, max(-1, self._vel_y + dy * fac))
            super().move()

    def check_health(self):
        if self._collected:
            if abs(self._vel_z) <= 0.1: self.die()

        elif self.overlaps(shared.game.player):
            shared.game.inventory.add(self._item)
            shared.sound.play("get_item")

            self._bound_to_world = False
            self.align_cx_b(self, shared.game.player)
            self._collected = True
            self.z = 10
            self._vel_z = 1
            self._acc = 0.9
            self._shadow = None

    def _move_x(self):
        self.xf += self._vel_x
        self._vel_x *= self._acc

    def _move_y(self):
        self.yf += self._vel_y
        self._vel_y *= self._acc

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass
