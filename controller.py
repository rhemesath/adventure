from typing import Callable, Optional
from abc import ABC, abstractmethod

from age import Keyboard, Gamepad

from constants import (
    BTN_LEFT, BTN_RIGHT, BTN_UP, BTN_DOWN, BTN_A, BTN_B,
    BTN_START, BTN_SELECT, BTN_L, BTN_R
)


class Controller(ABC):

    @abstractmethod
    def get(self, button: int) -> bool:
        pass

    @abstractmethod
    def down(self, button: int) -> bool:
        pass

    def update(self):
        pass

    def rumble(self, intensity: float, duration: float):
        pass

    def rumble_fx(self, func: Callable[[float], float], duration: Optional[float], intensity:float=1.0):
        pass

    def rumble_fx_done(self, func: Callable[[float], float]) -> bool:
        return True


class KeyboardController(Controller):
    def __init__(self, keyboard: Keyboard):
        self._keyboard = keyboard
        self._mapping = {
            BTN_LEFT:   "a",
            BTN_RIGHT:  "d",
            BTN_UP:     "w",
            BTN_DOWN:   "s",
            BTN_A:      "k",
            BTN_B:      "j",
            BTN_START:  "return",
            BTN_SELECT: "space",
            BTN_L:      "q",
            BTN_R:      "e"
        }

    def get(self, button: int) -> bool:
        return self._keyboard.is_pressed(self._mapping[button])

    def down(self, button: int) -> bool:
        return self._keyboard.down_event(self._mapping[button])


class GamepadController(Controller):
    def __init__(self, gamepad: Gamepad):
        self._gpad = gamepad
        self._mapping = {
            BTN_LEFT:   "DPAD_LEFT",
            BTN_RIGHT:  "DPAD_RIGHT",
            BTN_UP:     "DPAD_UP",
            BTN_DOWN:   "DPAD_DOWN",
            BTN_A:      "B",
            BTN_B:      "A",
            BTN_START:  "START",
            BTN_SELECT: "BACK",
            BTN_L:      "LEFTSHOULDER",
            BTN_R:      "RIGHTSHOULDER"
        }

        self._update_wait_frames = 6
        self._frame_counter = 0
        self._fx_func: Optional[Callable[[float], float]] = None
        self._fx_duration_secs = 0.0
        self._fx_intensity = 0.0

    def get(self, button: int) -> bool:
        return self._gpad.button(self._mapping[button])

    def down(self, button: int) -> bool:
        return self._gpad.button_down_event(self._mapping[button])

    def rumble(self, intensity: float, duration: float):
        self._gpad.rumble(intensity, intensity, duration)

    def rumble_fx(self, func: Callable[[float], float], duration: Optional[float], intensity:float=1.0):
        self._fx_func = func
        self._frame_counter = 0
        self._fx_duration_secs = duration
        self._fx_intensity = intensity

    def rumble_fx_done(self, func: Callable[[float], float]) -> bool:
        return self._fx_func is not func

    def update(self):
        if self._fx_func is None: return
        if self._frame_counter % self._update_wait_frames == 0:
            self._frame_counter += 1
            return

        t = self._frame_counter / 60
        val = self._fx_func(t)
        if self._fx_duration_secs is None and abs(val) < 0.005:
            self._fx_duration_secs = t

        if self._fx_duration_secs is not None and t >= self._fx_duration_secs:
            self.rumble(0, 0.001)
            self._fx_func = None
        else:
            self.rumble(val * self._fx_intensity, self._update_wait_frames / 60)

        self._frame_counter += 1
