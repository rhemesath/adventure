from typing import Dict, Optional

import age

from constants import MUSIC_DIR


class Music:
    def __init__(self, audio: age.Audio):
        self._musics: Dict[str, age.Music] = {}
        self._audio = audio

    def load_all(self):
        for music_path in MUSIC_DIR.iterdir():
            self._musics[music_path.stem] = age.Music.load(music_path)

    def play(self, name: Optional[str]):
        if name is None:
            self._audio.pause_music()
        else:
            self._audio.play_music(self._musics[name], loops=-1)
