from entity import Entity
from frame import Frame
from images import img_shadow_small


class UnlockParticle(Entity):
    def __init__(self, tile_x: int, tile_y: int):
        super().__init__(0, 0, w=8, h=8, shadow=img_shadow_small)
        self.center = tile_x * 16 + 8, tile_y * 16 + 8

        self._frame = Frame(0,9, 8,8)

        self._vel_x = 0.7
        self._vel_z = 2.4
        self._gravity = -0.1

        self._bound_to_world = False

    def set_frame(self):
        self._frame.angle += 5

    def check_health(self):
        if self.z == 0 and abs(self._vel_z) <= 0.1:
            self.die()

    def _move_x(self):
        self.xf += self._vel_x
    def _move_y(self):
        pass

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass
