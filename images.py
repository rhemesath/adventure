from frame import Frame as F
from animation import Animation, TileAnimation


img_empty = F(0, 0)

img_particle_leaves = F(2, 10)
img_particle_wood   = F(3, 10)
img_particle_stone  = F(4, 10)
img_particle_misc   = F(5, 10)

img_shadow       = F(0, 10.5, h=8, off_y=2)
img_shadow_small = F(0, 10, w=8, h=8, off_y=1)

img_entity_default = F(8, 11)

imgs_health = (F(2,9.5, 8,8), F(2,9, 8,8), F(1.5,9.5, 8,8), F(1.5,9, 8,8), F(1,9.5, 8,8))

ani_map_pointer = Animation((
    F(1,9, 8,8, off_x=0, off_y=-7), F(1,9, 8,8, off_x=1, off_y=-8)
), 20, loop=1)

ani_tile_water = TileAnimation((3, 11, 12, 11), 14)

ani_explosion = Animation((
    F(14,9, 32,32), F(12,9, 32,32), img_empty, img_empty, img_empty, img_empty, img_empty, img_empty,
    F(14,9, 32,32, flip_lr=1), F(12,9, 32,32, flip_tb=1), img_empty, img_empty, img_empty,
), 1)
ani_fire_explosion = Animation((
    F(10,9, 32,32), F(10,9, 32,32), F(10,11), F(11,11), F(12,11), F(13,11), F(14,11), F(15,11)
), 8)
ani_dust_particle = Animation((
    F(9,9, 8,8), F(9.5,9, 8,8), F(9,9.5, 8,8), F(9.5,9.5, 8,8)
), 5)

ani_despawn_smoke = Animation((
    F(10,12), F(11,12), F(12,12), F(13,12), F(14,12), F(15,12)
), 5)

ani_twinkle_particle = Animation((
    F(0.5,9, 8,8), F(0,9.5, 8,8), F(0.5,9.5, 8,8), F(0,9.5, 8,8)
), 2, loop=1)

ani_damage_swipe_r = Animation((
    F(7,12), F(8,12), F(9,12)
), 2)
ani_damage_swipe_l = Animation((
    F(7,12, flip_lr=1), F(8,12, flip_lr=1), F(9,12, flip_lr=1)
), 2)

img_water_droplet = F(8,9, 8,8)
ani_water_droplet = Animation((
    F(8.5,9, 8,8), F(8,9.5, 8,8), F(8.5,9.5, 8,8)
), 3)

ani_campfire = TileAnimation((26, 27), 10)
