from age import Texture

import shared
from rect import Rect
from images import imgs_health
from constants import BTN_SELECT


class Hud(Rect):
    def __init__(self):
        super().__init__(0, 0, shared.game.w, 16)

        self.tex = Texture.create(shared.game.w, shared.game.h)
        self.tex.set_blending(True)

    def render_tex(self):
        shared.paint.texture(self.tex, self.x, self.y,
            w=shared.game.w * shared.game.scale, h=shared.game.h * shared.game.scale
        )

    def render(self):
        with shared.paint.target_texture(self.tex):
            shared.paint.fill((0, 0, 0, 0))
            shared.paint.rect((30, 30, 60), *self.get_rect())

            shared.game.inventory.render()
            self._render()

            if shared.controller.get(BTN_SELECT): shared.game.world.render_minimap()

        self.render_tex()

    def _render(self):
        max_health = shared.game.player.max_health
        health = shared.game.player.health

        x = self.w - max_health // 4 * 8
        y = 1

        for _ in range(max_health // 4):
            shared.paint.texture(shared.main_tex, x, y,
                src=imgs_health[max(0, min(4, health))].get_rect()
            )
            health -= 4
            x += 8
