from math import exp
from random import uniform as rand

import shared
from rect import Rect


class Camera(Rect):
    def __init__(self, w: int, h: int):
        super().__init__(0, 0, w, h)
        self._hardness = 0.1
        self._off_x: float = 0
        self._off_y: float = 0
        self._shake_radius: float = 0
        self._shake_factor: float = 0

        self._shake_rumble_fx = ShakeRumbleFX()

    def shake(self, radius: float, factor:float=0.5):
        self._shake_radius = radius
        self._shake_factor = factor
        shared.controller.rumble_fx(self._shake_rumble_fx, None)

    def update(self, target: Rect, bounds: Rect):
        dest_x = target.xf + (target.w - self.w) // 2
        dest_y = target.yf + (target.h - self.h) // 2

        self.xf = self.xf * (1 - self._hardness) + dest_x * self._hardness
        self.yf = self.yf * (1 - self._hardness) + dest_y * self._hardness

        self.keep_inside(bounds)

        if self._shake_radius != 0:
            if abs(self._shake_radius) <= 0.1: self._shake_radius = 0
            self._off_x = rand(-self._shake_radius, self._shake_radius)
            self._off_y = rand(-self._shake_radius, self._shake_radius)
            self._shake_rumble_fx.radius = self._shake_radius
            self._shake_radius *= self._shake_factor

        shared.paint.set_cam_pos(
            int(self.xf + self._off_x),
            int(self.yf - shared.game.hud.h + self._off_y)
        )

    def render(self):
        game = shared.game
        tex = game.scroll_tex
        scale = game.scale

        x = self.xf + self._off_x
        y = self.yf + self._off_y

        shared.paint.texture(tex,
            int((int(x) - x) * scale),
            int((int(y) - y) * scale),
            w=tex.w * scale, h=tex.h * scale
        )


class ShakeRumbleFX:
    def __init__(self):
        self.radius = 0

    def __call__(self, t: float):
        return 1 - exp(-self.radius / 24) # max intensity at radius=24
