from entity import Entity
from images import ani_despawn_smoke, img_empty


class DespawnSmoke(Entity):
    def __init__(self, cx: int, cy: int):
        super().__init__(0, 0, 16, 16)
        self.center = (cx, cy)

        self._frame = img_empty

        self._counter = 0

    def move(self): pass

    def set_frame(self):
        self._frame = ani_despawn_smoke.get_frame(self._counter)
        self._counter += 1

    def check_health(self):
        if ani_despawn_smoke.is_done(self._counter):
            self.die()
