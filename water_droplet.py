from random import randint

import shared
from entity import Entity, State
from frame import Frame
from images import img_water_droplet, ani_water_droplet, img_empty
from constants import STATE_IDLE, STATE_MOVING, STATE_ANIMATION


class IdleState(State):
    name = STATE_IDLE

    def __init__(self, user: Entity):
        super().__init__(user)
        self._counter = 0

    def enter(self):
        self.counter = randint(0, 60 * 4)
        self._user.x = randint(shared.game.cam.x - 8, shared.game.cam.r)
        self._user.y = randint(shared.game.cam.y - 8, shared.game.cam.b)
        self._user.z = shared.game.h + 30

    def update(self):
        if self.counter == 0:
            self._user.change_state(self._user.move_state)
            return
        self.counter -= 1

    def get_frame(self) -> Frame:
        return img_empty


class MoveState(State):
    name = STATE_MOVING

    def update(self):
        if self._user.z <= 0:
            self._user.change_state(self._user.ani_state)

    def move(self):
        self._user.z -= 6

    def get_frame(self) -> Frame:
        return img_water_droplet


class AnimationState(State):
    name = STATE_ANIMATION

    def __init__(self, user: Entity):
        super().__init__(user)
        self._counter = 0
        self._ani = ani_water_droplet

    def enter(self):
        self._counter = 0

    def update(self):
        if self._ani.is_done(self._counter):
            self._user.change_state(self._user.idle_state)
            return
        self._counter += 1

    def get_frame(self) -> Frame:
        return self._ani.get_frame(self._counter)


class WaterDroplet(Entity):
    def __init__(self):
        super().__init__(0, 0, w=8, h=8)

        self.idle_state = IdleState(self)
        self.move_state = MoveState(self)
        self.ani_state  = AnimationState(self)

        self.change_state(self.idle_state)

    def update(self):
        self._state.update()

    def move(self):
        self._state.move()

    def set_frame(self):
        self._frame = self._state.get_frame()
