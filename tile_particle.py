from random import uniform as rand

from entity import Entity
from frame import Frame


class TileParticle(Entity):
    def __init__(self, img: Frame, tile_x: int, tile_y: int):
        super().__init__(tile_x * 16, tile_y * 16, 16, 16)

        self._frame = img

        self._vel_x = rand(-0.6, 0.6)
        self._vel_z = rand(1, 1.8)
        self._gravity = -0.1

        self._bound_to_world = False

    def check_health(self):
        if self.z == 0 and abs(self._vel_z) <= 0.1:
            self.die()

    def _move_x(self):
        self.xf += self._vel_x
    def _move_y(self):
        pass

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass
