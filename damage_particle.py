from typing import Optional, Tuple
from random import uniform as rand

import shared
from entity import Entity
from images import img_shadow_small, ani_damage_swipe_l, ani_damage_swipe_r, img_empty


class DamageParticle(Entity):
    def __init__(self, damage: int, cx: int, cy: int, color:Optional[Tuple[int, int, int]]=None):
        self._text  = str(-damage)
        self._color = color

        super().__init__(0, 0, *shared.game.text.size(self._text))
        self.center = (cx, cy)

        self._vel_x = rand(-1, 1)
        self._vel_y = rand(-1, 0)
        self._vel_z = 3
        self._gravity = -0.2

        self._bound_to_world = False

        self._shadow = img_shadow_small

    def move(self):
        super().move()

    def check_health(self):
        if self.z == 0 and abs(self._vel_z) <= 0.1:
            self.die()

    def _move_x(self):
        self.xf += self._vel_x
    def _move_y(self):
        self.yf += self._vel_y

    def _check_tile_col_x(self): pass
    def _check_tile_col_y(self): pass

    def render(self):
        self._render_shadow()
        shared.game.text.render(self._text, self.x, self.y - int(self.z), color=self._color)


class DamageSwipe(Entity):
    def __init__(self, user: Entity, left:bool=False):
        super().__init__(0, 0, 16, 16)

        self._user = user
        self._ani = ani_damage_swipe_l if left else ani_damage_swipe_r
        self._frame = img_empty

        self._counter = 0

        self.move()

    def set_frame(self):
        self._frame = self._ani.get_frame(self._counter)
        self._counter += 1

    def check_health(self):
        if self._ani.is_done(self._counter):
            self.die()

    def move(self):
        self.center = self._user.center
