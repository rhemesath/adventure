from typing import Dict

from animation import TileAnimation


class TileAnimator:
    def __init__(self):
        self._tiles: Dict["Tile", TileAnimation] = {}

        self._counter = 0

    def register(self, tile: "Tile", animation: TileAnimation):
        self._tiles[tile] = animation

    def update(self):
        for tile, animation in self._tiles.items():
            tile.gfx = animation.get_frame(self._counter)

        self._counter += 1


from tile import Tile
