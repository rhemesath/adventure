from typing import Optional, Union, Callable, Dict, ClassVar, List

from age import Texture

import shared
from constants import INF, TILE_LAYER
from item import Item
from frame import Frame
from animation import TileAnimation
from item_entity import ItemEntity
from texture_map import texture_map


class TileType:
    all_: ClassVar[Dict[str, "TileType"]] = {}

    interact: Optional[Callable[[int, int], bool]] = None

    def __init__(self, id_: str,
        layer:int=TILE_LAYER,
        solid:bool=False,
        health:int=-1,
        item:Optional[Item]=None,
        tile:Union["Tile", None, Dict[int, Optional["Tile"]]]=None,
        min_damage:int=0,
        particle:Optional[Frame]=None,
    ):
        self.id = id_
        if self.id != "": self._register(self)

        self.solid = solid
        self.layer = layer
        self.health = health
        self.min_damage = min_damage
        self.item: Optional[item]
        self.particle = particle
        self.tile: Dict[int, Optional[Tile]]
        self.set_item(item)
        self.set(tile)

    def get(self, damage:int=INF) -> Optional["Tile"]:
        for dmg, tile in self.tile.items():
            if damage >= dmg: return tile
        return tile

    def set(self, tile: Union["Tile", None, Dict[int, Optional["Tile"]]]):
        self.tile = {INF: tile} if type(tile) is not dict else tile

    def set_item(self, item: Optional[Item]):
        self.item = item

    def render_item(self, texture: Texture, x: int, y: int, size: int):
        if self.item is None: return
        paint = texture.get_painter_()
        paint.circle((255, 255, 255, 120), x + size, y + size, int(size / 2 * 1.4))
        paint.texture(texture, x+size//2, y+size//2, w=size, h=size, src=self.item.gfx.get_rect())

    @classmethod
    def _register(cls, tile_type: "TileType"):
        cls.all_[tile_type.id] = tile_type

    def __eq__(self, other: "TileType") -> bool:
        return self.id == other.id

    def __hash__(self) -> int:
        return hash(self.id)


class ContainerTileType(TileType):
    """ drops a given item when interacted with and then replaces itself """

    def interact(self, tile_x: int, tile_y: int) -> bool:
        if self.item is None: return False
        shared.sound.play("chest")
        shared.game.ent_man.add(ItemEntity(self.item, tile_x*16+8, tile_y*16+8))
        shared.game.world.set_tile_(self.get(), tile_x, tile_y)
        return False

    def set_item(self, item: Optional[Item]):
        super().set_item(item)
        self.id = f"container {'none' if item is None else item.name}"
        self._register(self)


class Tile:
    all_: ClassVar[Dict[int, "Tile"]] = {}
    aliases: ClassVar[Dict[int, List["Tile"]]] = {}

    @classmethod
    def new(cls, gfx: Union[int, TileAnimation], type_: Optional[TileType]) -> "Tile":
        if type_ is None and type(gfx) is int:
            type_ = TileType.all_.get(texture_map.get(gfx))
            if type_ is None:
                raise Exception(f"Tile gfx {gfx} is not mapped")

        new = cls(gfx, type_)
        h = hash(new)
        if h in cls.all_:
            return cls.all_[h]
        cls.all_[h] = new

        aliases = cls.aliases.get(new.gfx)
        if aliases is None:
            aliases = []
            cls.aliases[new.gfx] = aliases
        aliases.append(new)

        return new

    def __init__(self, gfx: Union[int, TileAnimation], type_: TileType):
        """ use new() instead! """
        self.type_ = type_

        self.gfx: int
        if type(gfx) is int:
            self.gfx = gfx
            self.id = self.gfx
        else:
            self.gfx = gfx.get_frame(0)
            self.id = self.gfx
            shared.tile_animator.register(self, gfx)

    def render(self, texture: Texture, x: int, y: int, size:int=16, _item:bool=False):
        tex_w = texture.w // 16
        texture.get_painter_().texture(texture, x, y, w=size, h=size, src=(
            (self.gfx  % tex_w) * 16,
            (self.gfx // tex_w) * 16,
            16, 16
        ))
        if _item: self.type_.render_item(texture, x, y, size // 2)

    def __eq__(self, other: "Tile") -> bool:
        return self.type_ == other.type_

    def __hash__(self) -> int:
        return hash((self.type_.id, self.id))


class ContainerTile(Tile):
    type_: ContainerTileType

    def render(self, texture: Texture, x: int, y: int, size:int=16):
        super().render(texture, x, y, size=size, _item=True)


class LockedTile(Tile):
    def render(self, texture: Texture, x: int, y: int, size:int=16):
        super().render(texture, x, y, size=size)
        self.type_.get().type_.render_item(texture, x, y, size // 2)
