from typing import Dict, Final
import json

from constants import TEXTURE_MAP_FILE


texture_map: Final[Dict[int, str]] = {}


def save_texture_map():
    with TEXTURE_MAP_FILE.open("w") as texture_map_file:
        json.dump(texture_map, texture_map_file)


def load_texture_map():
    with TEXTURE_MAP_FILE.open() as texture_map_file:
        texture_map.update({int(key): value for key, value in json.load(texture_map_file).items()})
