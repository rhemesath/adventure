from typing import Tuple

from constants import DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN


class Rect:
    def __init__(self, x: int, y: int, w: int, h: int):
        self.xf: float = x
        self.yf: float = y
        self.w = w
        self.h = h

    def get_rect(self) -> Tuple[int, int, int, int]:
        return (self.x, self.y, self.w, self.h)

    @property
    def x(self) -> int: return int(self.xf)
    @property
    def y(self) -> int: return int(self.yf)
    @property
    def r(self) -> int: return self.x + self.w
    @property
    def b(self) -> int: return self.y + self.h
    @property
    def center_x(self) -> int: return self.x + self.w // 2
    @property
    def center_y(self) -> int: return self.y + self.h // 2
    @property
    def center(self) -> Tuple[int, int]: return (self.center_x, self.center_y)
    @property
    def tile_x(self) -> int: return self.x // 16
    @property
    def tile_y(self) -> int: return self.y // 16
    @property
    def tile_r(self) -> int: return (self.r - 1) // 16
    @property
    def tile_b(self) -> int: return (self.b - 1) // 16
    @property
    def tile_ri(self) -> int: return self.r // 16
    @property
    def tile_bi(self) -> int: return self.b // 16
    @property
    def size(self) -> Tuple[int, int]: return (self.w, self.h)

    @x.setter
    def x(self, x: int): self.xf = x
    @y.setter
    def y(self, y: int): self.yf = y
    @r.setter
    def r(self, r: int): self.x = r - self.w
    @b.setter
    def b(self, b: int): self.y = b - self.h
    @center_x.setter
    def center_x(self, center_x: int): self.x = center_x - self.w // 2
    @center_y.setter
    def center_y(self, center_y: int): self.y = center_y - self.h // 2
    @center.setter
    def center(self, center: Tuple[int, int]): self.center_x, self.center_y = center
    @tile_x.setter
    def tile_x(self, tile_x: int): self.x = tile_x * 16
    @tile_y.setter
    def tile_y(self, tile_y: int): self.y = tile_y * 16
    @tile_r.setter
    def tile_r(self, tile_r: int): self.r = (tile_r + 1) * 16
    @tile_b.setter
    def tile_b(self, tile_b: int): self.b = (tile_b + 1) * 16
    @tile_ri.setter
    def tile_ri(self, tile_ri: int): self.r = tile_ri * 16
    @tile_bi.setter
    def tile_bi(self, tile_bi: int): self.b = tile_bi * 16
    @size.setter
    def size(self, size: Tuple[int, int]): self.w, self.h = size

    def keep_inside(self, rect: "Rect"):
        if   self.x < rect.x: self.x = rect.x
        elif self.r > rect.r: self.r = rect.r
        if   self.y < rect.y: self.y = rect.y
        elif self.b > rect.b: self.b = rect.b

    def align(self, anchor: "Rect", cross_dir: int):
        if cross_dir == DIR_LEFT:
            self.r = anchor.x
            self.center_y = anchor.center_y
        elif cross_dir == DIR_RIGHT:
            self.x = anchor.r
            self.center_y = anchor.center_y
        elif cross_dir == DIR_UP:
            self.center_x = anchor.center_x
            self.b = anchor.y
        elif cross_dir == DIR_DOWN:
            self.center_x = anchor.center_x
            self.y = anchor.b

    def overlaps(self, rect: "Rect"):
        return (
            self.x < rect.r and self.r > rect.x and
            self.y < rect.b and self.b > rect.y
        )

    def point_inside(self, x: int, y: int) -> bool:
        return (self.x <= x < self.r) and (self.y <= y < self.b)

    @staticmethod
    def align_none(rect: "Rect", anchor: "Rect"):
        rect.x, rect.y = anchor.x, anchor.y
    @staticmethod
    def align_c(rect: "Rect", anchor: "Rect"):
        rect.center = anchor.center
    @staticmethod
    def align_cx_b(rect: "Rect", anchor: "Rect"):
        rect.center_x = anchor.center_x
        rect.b = anchor.b
