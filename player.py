from typing import Dict, Tuple, Union, Optional
from math import sqrt

import shared
from entity import Entity, State, KnockbackState
from action_rect import UseActionRect, AttackRect
from frame import Frame as F
from animation import Animation
from images import img_shadow
from damage_particle import DamageParticle
from constants import (
    BTN_LEFT, BTN_RIGHT, BTN_UP, BTN_DOWN, BTN_A, BTN_B,

    DIR_LEFT, DIR_RIGHT, DIR_UP, DIR_DOWN,
    STATE_IDLE, STATE_MOVING, STATE_ATTACKING, STATE_ACTION,
    STATE_KNOCKBACK,

    HIT
)
from tiles import LADDER_DOWN, LADDER_UP, HOLE_DOWN, CAMPFIRE_ON


class IdleState(State):
    name = STATE_IDLE

    def update(self):
        if shared.controller.down(BTN_B):
            self._user.change_state(self._user.attack_state)

        elif shared.controller.down(BTN_A):
            self._user.change_state(self._user.item_use_state)

        elif any(shared.controller.get(btn) for btn in (BTN_LEFT, BTN_RIGHT, BTN_UP, BTN_DOWN)):
            self._user.change_state(self._user.move_state)


class MoveState(State):
    name = STATE_MOVING

    def update(self):
        if shared.controller.down(BTN_B):
            self._user.change_state(self._user.attack_state)

        elif shared.controller.down(BTN_A):
            self._user.change_state(self._user.item_use_state)

        elif not any(shared.controller.get(btn) for btn in (BTN_LEFT, BTN_RIGHT, BTN_UP, BTN_DOWN)):
            self._user.change_state(self._user.idle_state)

    def move(self):
        dir_x = dir_y = 0.0

        if shared.controller.get(BTN_LEFT):  dir_x = -1
        if shared.controller.get(BTN_RIGHT): dir_x =  1
        if shared.controller.get(BTN_UP):    dir_y = -1
        if shared.controller.get(BTN_DOWN):  dir_y =  1

        diag = 1 / sqrt(2)
        if dir_x != 0 and dir_y != 0:
            dir_x *= diag
            dir_y *= diag

        self._user.dir_x = dir_x
        self._user.dir_y = dir_y

    def move_x(self):
        self._user.xf += self._user.vel * self._user.dir_x
    def move_y(self):
        self._user.yf += self._user.vel * self._user.dir_y


class AttackState(State):
    name = STATE_ATTACKING

    def enter(self):
        self._user.ani_frame_counter = 0
        self._user.attack_rect.use(shared.game.inventory.item, self._user.cross_dir)

        self._user.dir_x = 0
        self._user.dir_y = 0
        if shared.controller.get(BTN_LEFT):  self._user.dir_x = -1
        if shared.controller.get(BTN_RIGHT): self._user.dir_x =  1
        if shared.controller.get(BTN_UP):    self._user.dir_y = -1
        if shared.controller.get(BTN_DOWN):  self._user.dir_y =  1

    def update(self):
        if shared.controller.down(BTN_B):
            self._user.change_state(self)

        ani = self._user.imgs[(self._user.cross_dir, self.name)]
        if ani.is_done(self._user.ani_frame_counter):
            self._user.change_state(self._user.idle_state)


class ItemUseState(State):
    name = STATE_ACTION

    def enter(self):
        self._user.ani_frame_counter = 0
        self._user.use_action_rect.use(shared.game.inventory.item, self._user.cross_dir)

        self._user.dir_x = 0
        self._user.dir_y = 0
        if shared.controller.get(BTN_LEFT):  self._user.dir_x = -1
        if shared.controller.get(BTN_RIGHT): self._user.dir_x =  1
        if shared.controller.get(BTN_UP):    self._user.dir_y = -1
        if shared.controller.get(BTN_DOWN):  self._user.dir_y =  1

    def update(self):
        ani = self._user.imgs[(self._user.cross_dir, self.name)]
        if ani.is_done(self._user.ani_frame_counter):
            self._user.change_state(self._user.idle_state)


class Player(Entity):
    def __init__(self, x: int, y: int):
        super().__init__(x, y, w=10, h=14, shadow=img_shadow)

        self.idle_state      = IdleState(self)
        self.move_state      = MoveState(self)
        self.attack_state    = AttackState(self)
        self.item_use_state  = ItemUseState(self)
        self.knockback_state = KnockbackState(self)

        self.tile_cx = 0
        self.tile_cy = 0
        self.prev_tile_x = 0
        self.prev_tile_y = 0

        self.cross_dir = DIR_DOWN
        self.dir_x = 0.0
        self.dir_y = 0.0

        self.vel: float = 1.6

        self.use_action_rect = UseActionRect(self)
        self.attack_rect     = AttackRect(self)

        self.light_radius = 32

        self.check_collisions = True
        self.has_health = True
        self._invulnerability_frames = 1 * 60
        self.max_health = 4
        self.health = self.max_health

        move_dur = 6
        attack_dur = 2
        imgs: Dict[Tuple[int, int], Union[F, Animation]] = {
            (DIR_DOWN, STATE_IDLE): F(0, 15),
            (DIR_DOWN, STATE_MOVING): Animation((
                F(1, 15, off_y=-1), F(0, 15),
                F(2, 15, off_y=-1), F(0, 15)), move_dur, loop=1),
            (DIR_DOWN, STATE_ATTACKING): Animation((
                F(0, 15), F(3, 15, off_y=-1), F(3, 15, off_y=-1), F(4, 15, off_y=1)
            ), attack_dur),

            (DIR_UP, STATE_IDLE): F(0, 14),
            (DIR_UP, STATE_MOVING): Animation((
                F(1, 14, off_y=-1), F(0, 14),
                F(2, 14, off_y=-1), F(0, 14)), move_dur, loop=1),
            (DIR_UP, STATE_ATTACKING): Animation((
                F(0, 14), F(3, 14, off_y=1), F(3, 14, off_y=1), F(4, 14, off_y=-1)
            ), attack_dur),

            (DIR_RIGHT, STATE_IDLE): F(0, 13),
            (DIR_RIGHT, STATE_MOVING): Animation((
                F(1, 13, off_y=-1), F(0, 13),
                F(2, 13, off_y=-1), F(0, 13)), move_dur, loop=1
            ),
            (DIR_RIGHT, STATE_ATTACKING): Animation((
                F(0, 13), F(3, 13, off_x=-1), F(3, 13, off_x=-1), F(4, 13, off_x=1)
            ), attack_dur),

            (DIR_LEFT, STATE_IDLE): F(0, 13, flip_lr=1),
            (DIR_LEFT, STATE_MOVING): Animation((
                F(1, 13, off_y=-1, flip_lr=1), F(0, 13, flip_lr=1),
                F(2, 13, off_y=-1, flip_lr=1), F(0, 13, flip_lr=1)), move_dur, loop=1
            ),
            (DIR_LEFT, STATE_ATTACKING): Animation((
                F(0, 13, flip_lr=1), F(3, 13, off_x=1, flip_lr=1), F(3, 13, off_x=1, flip_lr=1), F(4, 13, off_x=-1, flip_lr=1)
            ), attack_dur),
        }
        imgs.update({
            (DIR_DOWN,  STATE_ACTION): imgs[(DIR_DOWN,  STATE_ATTACKING)],
            (DIR_UP,    STATE_ACTION): imgs[(DIR_UP,    STATE_ATTACKING)],
            (DIR_RIGHT, STATE_ACTION): imgs[(DIR_RIGHT, STATE_ATTACKING)],
            (DIR_LEFT,  STATE_ACTION): imgs[(DIR_LEFT,  STATE_ATTACKING)],

            (DIR_DOWN,  STATE_KNOCKBACK): imgs[(DIR_DOWN,  STATE_IDLE)],
            (DIR_UP,    STATE_KNOCKBACK): imgs[(DIR_UP,    STATE_IDLE)],
            (DIR_RIGHT, STATE_KNOCKBACK): imgs[(DIR_RIGHT, STATE_IDLE)],
            (DIR_LEFT,  STATE_KNOCKBACK): imgs[(DIR_LEFT,  STATE_IDLE)]
        })
        self.imgs = imgs
        self.ani_frame_counter = 0
        self._img_align = self.align_cx_b

        self.change_state(self.idle_state)

    def spawn(self):
        shared.game.ent_man.add(self)
        shared.game.ent_man.add(self.use_action_rect)
        shared.game.ent_man.add(self.attack_rect)

    def die(self):
        shared.sound.play("entity_die")
        super().die()

    def update(self):
        self._state.update()

    def change_state(self, state: State):
        if self._state is not None and state.name != self._state.name:
            self.ani_frame_counter = 0
        super().change_state(state)

    def action(self):
        self._state.action()

        # check tile events
        tile_x, tile_y = self.tile_cx, self.tile_cy
        if tile_x == self.prev_tile_x and tile_y == self.prev_tile_y: # did not move
            return

        world = shared.game.world
        tile = world.get_real_tile(tile_x, tile_y)

        if tile is LADDER_DOWN or tile is HOLE_DOWN:
            world.change_level(1)

        elif tile is LADDER_UP:
            world.change_level(-1)
            if world.get_real_tile(tile_x, tile_y) is HOLE_DOWN:
                world.set_tile(LADDER_DOWN, tile_x, tile_y)

        elif tile is CAMPFIRE_ON:
            self.take_damage(None, 1)

        else: return

        self.center_x = tile_x * 16 + 8
        self.tile_b = tile_y

    def set_frame(self):
        img = self.imgs[(self.cross_dir, self._state.name)]
        if type(img) is Animation:
            self._frame = img.get_frame(self.ani_frame_counter)
            self.ani_frame_counter += 1
        else:
            self._frame = img

    def check_health(self):
        super().check_health()
        if self.health <= 0:
            self.die()

    def take_damage(self, from_entity: Optional["Entity"], damage: int):
        prev_health = self.health
        v = super().take_damage(from_entity, damage)
        if v == HIT:
            shared.sound.play("player_hurt")
            shared.game.ent_man.add(DamageParticle(prev_health - self.health, self.center_x, self.y, color=(255, 0, 0)))
            if from_entity is not None:
                self.change_state(self.knockback_state)
                self.knockback_state.set_knockback(from_entity)
        return v

    def move(self):
        self._state.move()
        super().move()
        # choose cross direction
        lr = ud = False
        if self.dir_x != 0 and self.dir_y != 0: # diagonal movement
            if self.cross_dir in (DIR_LEFT, DIR_RIGHT): lr = True
            else: ud = True
        elif self.dir_x != 0: lr = True
        elif self.dir_y != 0: ud = True
        if lr: self.cross_dir = DIR_LEFT if self.dir_x < 0 else DIR_RIGHT
        elif ud: self.cross_dir = DIR_UP if self.dir_y < 0 else DIR_DOWN

        self.prev_tile_x = self.tile_cx
        self.prev_tile_y = self.tile_cy
        self.tile_cx = self.center_x // 16
        self.tile_cy = self.center_y // 16

    def _move_x(self): self._state.move_x()
    def _move_y(self): self._state.move_y()

    def render(self):
        super().render()
