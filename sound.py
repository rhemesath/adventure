from typing import Dict

import age

from constants import SOUNDS_DIR


class Sound:
    def __init__(self, audio: age.Audio):
        self._sounds: Dict[str, age.Sound] = {}
        self._audio = audio

    def load_all(self):
        for sound_path in SOUNDS_DIR.iterdir():
            self._sounds[sound_path.stem] = age.Sound.load(sound_path)

    def play(self, name: str):
        self._audio.play_sound(self._sounds[name])
