from entity import Entity


class LightSource(Entity):
    def __init__(self, cx: int, cy: int, radius: int):
        super().__init__(0, 0, 1, 1)
        self.center = (cx, cy)

        self.light_radius = radius

    def check_health(self): pass
    def move(self): pass
    def render(self): pass
