Adventure Game
==============

** WIP **

## Installation ##

Requirements:

* Python 3
* PySDL2
* SDL2, SDL2_mixer, SDL2_image

Clone this repository.
If you don't have GIT installed, download this repository and unzip it instead.

### Linux ###

First, install Python 3, and the SDL2 binaries

On Debian based systems:
```shell
sudo apt install python3 python3-pip libsdl2-2.0-0 libsdl2-mixer-2.0-0 libsdl2-image-2.0-0
```

On Arch based systems:
```shell
sudo pacman -S python python-pip sdl2 sdl2_mixer sdl2_image
```

After that, install PySDL2
```shell
pip3 install pysdl2
```

### Windows ###

Download the 64-Bit Windows installer for Python 3 [here](https://www.python.org)
and install it.

After that, open the Command Prompt (search `cmd` in your start menu)  
and install PySDL2 and the SDL2 binaries:
```cmd
py -m pip install pysdl2 pysdl2-dll
```

## Start the game ##

On Windows, double-click the `game.py` file.
If you want more options, open the Command Prompt and do it like below,
but just type `py` instead of `python3`. But before that, use the `cd` command
to go into this directory (where this README.md is located)

To start the game:
```shell
python3 game.py
```

To see available options:
```shell
python3 game.py -h
```

The `-d` option is for debugging. Currently, it adds a one-hit-item and
some bombs

## Controlls ##

By default, the keyboard is used for input

* Move with `W` `A` `S` `D`
* Switch to left / right item with `Q` and `E`
* Attack with current item with `J`
* Use item with `K`
* Hold `SPACE` to show a minimap

`ESC` closes the game

### Gamepads ###

Most common game controllers are supported.
To use yours, connect it and do:
```shell
python3 game.py -g 0
```

If the game starts but the controller does nothing,
list all connected controllers, to see if yours is compatible with the game:
```shell
python3 game.py -l
```

This enumerates all controllers. Use the number on the left of the controller
you want to use when starting the game:
```shell
python3 game.py -g NUMBER
```

If your gamepad didn't show up on the list, try another gamepad or use the keyboard :)


The controlls here are like this:
* Move with the `d-pad` or the `left analog stick`
* Switch to left / right item with the `left/right shoulder buttons`
* Hold the left menu button (`SELECT` / `-` / `BACK` / `SHARE`) to show a minimap

```text
  O
O   O --- Use item / interact
  O
  |
  Attack with current item
```

## Gameplay ##

There is not much to do at the moment. Try to find the cave entrance, explore it, and try to get out of there.

You can use the pickaxe to interact with stone and make some bricks.

Have fun! :)

## Problems ##

### The game runs too fast ###
The game's speed is bound to the display's refresh rate (pretty old school, right?)  
If your display's refresh rate is greater than 60Hz, go to the display settings on your OS and set it to 60Hz.

If the game runs too slow, you either have your display set to less than 60Hz or you should really check what else
is eating up all the CPU on your device.

### The window content is too small/large ###
Use the pixel scale parameter `-s` to adjust the scaling:

```shell
python3 game.py -s SCALE
```

The default is `4`, meaning one in-game pixel is `4x4` pixels in size on your display.

On most laptops `3` is a good fit. On 4K monitors, try `6`.
